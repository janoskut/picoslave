function(git_version OUTPUT_VARIABLE)
    set(VERSION "unknown")
    execute_process(
        COMMAND git describe --tags --always --dirty=+dirty
        OUTPUT_VARIABLE GIT_DESCRIBE_OUTPUT
        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
        ERROR_VARIABLE GIT_DESCRIBE_ERROR
        OUTPUT_STRIP_TRAILING_WHITESPACE)

    if(NOT GIT_DESCRIBE_ERROR STREQUAL "")
        message(WARNING "Git describe error: ${GIT_DESCRIBE_ERROR}")
    elseif(GIT_DESCRIBE_OUTPUT STREQUAL "")
        message(WARNING "GIT_VERSION=\"${VERSION}\"")
    else()
        message(STATUS "GIT_VERSION=${GIT_DESCRIBE_OUTPUT}")
        set(VERSION ${GIT_DESCRIBE_OUTPUT})
    endif()

    set(${OUTPUT_VARIABLE}
        ${VERSION}
        PARENT_SCOPE)
endfunction()
