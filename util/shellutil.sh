#!/usr/bin/env bash

([[ -n $ZSH_EVAL_CONTEXT && $ZSH_EVAL_CONTEXT =~ :file$ ]] ||
 [[ -n $KSH_VERSION && $(cd "$(dirname -- "$0")" &&
    printf '%s' "${PWD%/}/")$(basename -- "$0") != "${.sh.file}" ]] ||
 [[ -n $BASH_VERSION ]] && (return 0 2>/dev/null)) && sourced=1

function _print_path_check() {
    printf "\033[1;33mEnvironment check\033[0m:\n"

    printf "    cmake:"
    cmakePath="$(which cmake)"
    if [ $? = 0 ]; then
        printf "         \033[0;32m%s\033[0m\n" "${cmakePath}"
    else
        printf "         \033[0;31mnot found\033[0m\n"
    fi

    printf "    openocd:"
    openocdPath="$(which openocd)"
    if [ $? = 0 ]; then
        printf "       \033[0;32m%s\033[0m\n" "${openocdPath}"
    else
        printf "       \033[0;31mnot found\033[0m\n"
    fi

    printf "    PICO_SDK_PATH:"
    if [ -n "${PICO_SDK_PATH}" ]; then
        printf " \033[0;32m%s\033[0m\n" "${PICO_SDK_PATH}"
    else
        printf " \033[0;31mnot set\033[0m\n"
    fi
}

function _print_help() {
    printf "\033[1;34m---------\033[0m\n"
    printf "\033[1;34mPicoSlave\033[0m \033[1;33mshell\033[0m \033[1;32mfunctions\033[0m:\n"
    printf "\033[1;34m---------\033[0m\n"
    printf "    \033[1;32mhelp\033[0m:                show this help\033[0m\n"
    printf "    \033[1;32mconfigure \033[0;90m[R]\033[0m:       'cmake -S . -B build' \033[0;90m(R for Release, else Debug)\033[0m\n"
    printf "    \033[1;32mbuild \033[0;90m[R]\033[0m:           'cmake --build build -j\$(nproc)'\n"
    printf "    \033[1;32mrebuild \033[0;90m[R]\033[0m:         'rm ./build && \033[0;32mconfigure\033[0m && \033[0;32mbuild\033[0m'\n"
    printf "    \033[1;32mflash\033[0m:               './picoflash.sh build/picoslave.elf'\n"
    printf "    \033[1;32mreflash\033[0m:             '\033[0;32mbuild\033[0m && \033[0;32mflash\033[0m'\n"
    printf "    \033[1;32mreset\033[0m:               './picoflash.sh -r'\n"
    printf "    \033[1;32mrecover \033[0;90m[<device>]\033[0m:  unbind/bind the USB device\n"
    printf "    \033[1;32msync \033[0;90muser@target\033[0m:    \`rsync\` continuously with a remote target\n"
    printf "    \033[1;32mrflash \033[0;90muser@target\033[0m:  run remote openocd flash command\n"
    printf "    \033[1;32mrun \033[0;90mCOUNT CMD...\033[0m:    repeat CMD until failure (or ctrl+c)\n"
}

if [ -z "${sourced}" ]; then
    >&2 printf "I want to be \033[1;33msourced\033[0m: '\033[0;91msource %s\033[0m'\n" "${0}"
    exit 1
else
    _print_help
    _print_path_check
fi

function _check_root_path() {
    # use this scripts location to determine if we're exeting from the project root
    if [ ! -f "./util/shellutil.sh" ]; then
        >&2 echo "ERROR: this shellutil functions must be executed from the PicoSlave project root path"
        return 1
    fi

}

function help() {
    _print_help
}

function configure() {
    _check_root_path || return 1
    TYPE="Debug"
    if [ "${1}" = "R" ]; then
        TYPE="Release"
        shift
    fi
    echo "rm -rf ./build && cmake -S . -B build -DCMAKE_BUILD_TYPE=${TYPE}" "$@"
    rm -rf ./build && cmake -S . -B build -DCMAKE_BUILD_TYPE="${TYPE}" "$@"
}

function build() {
    _check_root_path || return 1
    if [ ! -d "build" ]; then
        >&2 echo "ERROR: no \"./build\" directory - run 'configure' first"
        return 1
    fi
    echo "cmake --build build -j$(nproc)"
    cmake --build build -j"$(nproc)"
}

function rebuild() {
    _check_root_path || return 1
    configure "$@" && build
}

function flash() {
    _check_root_path || return 1
    if [ ! -f "./build/picoslave.elf" ]; then
        >&2 echo "ERROR: have to 'build' or 'rebuild' first"
        return 1
    fi
    ./util/picoflash.sh build/picoslave.elf
}

function rflash() {
    _check_root_path || return
    if [ -z "${1}" ]; then
        >&2 echo "ERROR: need target, e.g. pi@raspberry"
        return
    fi
    if [ ! -f "./build/picoslave.elf" ]; then
        >&2 echo "ERROR: have to 'build' or 'rebuild' first"
        return
    fi
    if ! ssh "${1}" "which openocd" > /dev/null; then
        >&2 echo "ERROR: openocd not found on remote target"
        return 1
    fi
    if ! scp "./build/picoslave.elf" "${1}:/tmp"; then
        >&2 echo "ERROR: couldn't copy ./build/picoslave.elf to target"
        return 1
    fi
    COMMANDS="-c \"program /tmp/picoslave.elf verify reset exit\""
    OPENOCD_CMD="openocd -f interface/picoprobe.cfg -f target/rp2040.cfg ${COMMANDS}"
    echo "running remote openocd: ${OPENOCD_CMD}"
    # shellcheck disable=SC2029
    if ! ssh "${1}" "${OPENOCD_CMD}"; then
        >&2 echo "ERROR: failed to flash target"
    fi
}

function reflash() {
    _check_root_path || return 1
    build "${1}" && flash
}

function reset() {
    _check_root_path || return 1
    ./util/picoflash.sh -r
}

function recover() {
    DEVICE="0000:05:00.3"
    if [ -n "${1}" ]; then
        DEVICE="${1}"
    fi
    echo "echo -n ${DEVICE} | sudo tee /sys/bus/pci/drivers/xhci_hcd/unbind"
    echo -n "${DEVICE}" | sudo tee /sys/bus/pci/drivers/xhci_hcd/unbind > /dev/null || return 1
    sleep 0.2
    echo "echo -n ${DEVICE} | sudo tee /sys/bus/pci/drivers/xhci_hcd/bind"
    echo -n "${DEVICE}" | sudo tee /sys/bus/pci/drivers/xhci_hcd/bind > /dev/null || return 1
}

function sync() {
    _check_root_path || return 1
    if [ -z "${1}" ]; then
        >&2 echo "ERROR: need target, e.g. pi@raspberry"
        return 1
    fi

    if ! command -v inotifywait &> /dev/null; then
        >&2 echo "ERROR: inotifywait not found. Install with: sudo apt install inotify-tools"
        return 1
    fi

    echo "rsync'ing with ${1}:picoslave ..."
    trap 'return 0' INT

    printf "\033[0;31mSyncing...\033[0m"
    rsync -a . "${1}:picoslave" --exclude-from=.rsyncignore --delete || return 1
    printf "\r\033[0;32mSync'ed.\033[0m       \n"

    while inotifywait -rq -e modify,create,delete,move --exclude "(\.git|\.venv|\.tox)" ./ ; do
        printf "\033[0;31mSyncing...\033[0m"
        rsync -a . "${1}:picoslave" --exclude-from=.rsyncignore --delete || return 1
        printf "\r\033[0;32mSync'ed.\033[0m       \n"
    done
}

function run() {
    if [ "$#" -lt 2 ]; then
        >&2 echo "ERROR: usage: run COUNT CMDs..."
        return 1
    fi
    count="${1}"
    shift
    i=1
    while "$@"; do
        printf "\033[0;33mrun: #%d\%d: success\033[0m\n" "${i}" "${count}"
        if [ "${i}" -ge "${count}" ]; then
            printf "\033[0;32mrun: finished %d runs\033[0m\n" "${i}"
            return 0
        fi
        ((i++))
    done
    >&2 printf "\033[0;31mrun: failed at #%d\033[0m\n" "${i}"
    return 1
}
