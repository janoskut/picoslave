#!/usr/bin/env bash

function usage() {
    echo "Usage: $(basename "${0}") [-r][--reset][-h][--help] [<filename.elf>]"
}

if [ $# -ne 1 ]; then
    >&2 usage
    exit 1
fi

if [ -z "$(which openocd)" ]; then
    >&2 echo "ERROR: 'openocd' not found"
    exit 1
fi

ARG="${1}"

if [ "${ARG}" = "-h" ] || [ "${ARG}" = "--help" ]; then
    usage
    exit 0
fi

if [ "${ARG}" = "-r" ] || [ "${ARG}" = "--reset" ]; then
    COMMANDS="-c \"init\" -c \"reset run\" -c \"exit\""
else
    if [ ! -f "${ARG}" ]; then
        >&2 echo "ERROR: no such file: ${ARG}"
        exit 1
    fi
    COMMANDS="-c \"program ${ARG} verify reset exit\""
fi

OPENOCD_CMD="openocd -f interface/picoprobe.cfg -f target/rp2040.cfg ${COMMANDS}"

printf "\033[0;32m%s\033[0m\n" "${OPENOCD_CMD}"
sh -c "${OPENOCD_CMD}"
