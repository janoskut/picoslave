FROM alpine:3.14

RUN apk add git make cmake gcc g++ python3 gcc-arm-none-eabi newlib-arm-none-eabi cppcheck

ARG PICO_SDK=/opt/pico-sdk
RUN git clone --depth 1 -b master https://github.com/raspberrypi/pico-sdk.git ${PICO_SDK}
RUN git -C ${PICO_SDK} submodule update --init
ENV PICO_SDK_PATH=${PICO_SDK}
