from . import version

__version__ = version.get(local_flag='.local')
