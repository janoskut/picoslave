from typing import Any

from unittest.mock import patch, MagicMock

import pytest

from picoslave.picoslave import UsbPicoSlave, PicoSlave


class DeviceMock():

    def __init__(self, serial_number: str) -> None:
        self.serial_number = serial_number
        self.active_config = object()
        self.bus = 0
        self.address = 0
        self.port_numbers = [0]
        self.manufacturer = UsbPicoSlave.ID_VENDOR
        self.product = UsbPicoSlave.ID_PRODUCT
        self.reset = MagicMock()

    def __str__(self) -> str:
        return f'<{self.serial_number}>'

    def get_active_configuration(self) -> Any:
        return self.active_config


@pytest.fixture()
def device() -> DeviceMock:
    return DeviceMock('abc')


@pytest.fixture()
def outep() -> MagicMock:
    return MagicMock()


@pytest.fixture()
def inep() -> MagicMock:
    return MagicMock()


@pytest.fixture()
@patch('usb.util.find_descriptor')
def usb_picoslave(find_descriptor: MagicMock, device: DeviceMock,
                  outep: MagicMock, inep: MagicMock) -> UsbPicoSlave:
    find_descriptor.side_effect = [object(), outep, inep]
    return UsbPicoSlave(device)


@pytest.fixture()
@patch('usb.util.find_descriptor')
@patch('usb.core.find')
def picoslave(find: MagicMock, find_descriptor: MagicMock, device: DeviceMock,
              outep: MagicMock, inep: MagicMock) -> PicoSlave:
    find.return_value = [device]
    find_descriptor.side_effect = [object(), outep, inep]
    return PicoSlave()
