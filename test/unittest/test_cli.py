from unittest.mock import patch

import pytest
from pytest import CaptureFixture

from picoslave import __main__ as picocli


def test_help_maxlength(capsys: CaptureFixture[str]) -> None:

    with patch('sys.argv', ['picoslave', '-h']):
        with pytest.raises(SystemExit) as exc_info:
            picocli.main()

        assert exc_info.value.code == 0

    lines = capsys.readouterr().out.splitlines()
    for line in lines[2:]:
        assert len(line) <= 120, f'help line too long: {line}'


@pytest.mark.parametrize('cmd', [
    'config', 'read', 'write', 'clear', 'stat', 'scan', 'info', 'reset',
])
def test_subcmd_maxlength(capsys: CaptureFixture[str], cmd: str) -> None:

    with patch('sys.argv', ['picoslave', cmd, '-h']):
        with pytest.raises(SystemExit) as exc_info:
            picocli.main()

        assert exc_info.value.code == 0

    lines = capsys.readouterr().out.splitlines()
    for line in lines[2:]:
        assert len(line) <= 120, f'help line too long for sub cmd {cmd}: {line}'
