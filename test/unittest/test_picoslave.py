
import pytest
import struct
from typing import List, Optional, Tuple, Union

from unittest.mock import MagicMock, patch

from usb.core import USBError

from picoslave.picoslave import (
    PicoSlave, ProtocolError, ProtocolVersionError, ProtocolVersionWarning, UsbError
)

from .fixtures import *  # noqa


CMD = PicoSlave.Protocol.Command
CODE = PicoSlave.Protocol.ResponseCode


def make_response(
    header: Union[bytes, List[int]] = [],
    code: CODE = CODE.OK,
    data: Union[bytes, List[int]] = [],
    packet: Union[bytes, List[int]] = [],
    crc: Union[bytes, List[int]] = [],
) -> bytes:
    packet = bytes(packet) or (
        struct.pack('B', code)
        + struct.pack('<H', len(data))
        + bytes(data)
    )
    crc = bytes(crc) or struct.pack('<H', PicoSlave.Protocol.crc16(packet))
    header = bytes(header) or struct.pack('<I', len(packet) + len(crc))
    return header + packet + crc


def make_command(
    cmd: CMD,
    iface: int = 0,
    addr: int = 0x00,
    data: Union[bytes, List[int]] = [],
    size: Optional[int] = None,
) -> bytes:
    packet = (
        struct.pack('<B', cmd)
        + struct.pack('<B', iface)
        + struct.pack('<H', addr)
        + struct.pack('<H', size or len(data))
        + bytes(data)
    )
    crc = struct.pack('<H', PicoSlave.Protocol.crc16(packet))
    header = struct.pack('<I', len(packet) + len(crc))
    return header + packet + crc


def test_reset(picoslave: PicoSlave, outep: MagicMock, inep: MagicMock) -> None:
    inep.read.return_value = make_response()
    picoslave.reset()
    outep.write.assert_called_once_with(make_command(cmd=CMD.RESET))


def test_reset_write_error(picoslave: PicoSlave, outep: MagicMock) -> None:
    outep.write.side_effect = UsbError('write error')
    picoslave.reset()


def test_reset_read_error(picoslave: PicoSlave, inep: MagicMock) -> None:
    inep.read.side_effect = UsbError('read error')
    picoslave.reset()


@pytest.mark.parametrize('response, error_msg', [
    (make_response(code=CODE.INVALID_INTERFACE), 'received error code 4 (INVALID_INTERFACE)'),
    (make_response(code=CODE.INVALID_REQUEST), 'received error code 3 (INVALID_REQUEST)'),
    (make_response(data=[0xAB, 0xCD]), 'RESET received an unexpected response: AB CD'),
    (make_response(header=[0, 0, 0, 0]), 'received wrong number of bytes'),
    (make_response(packet=[0, 0]), 'invalid received packet size'),
    (make_response(packet=[0, 1, 0, 0xaa, 0xbb]), 'received packet size mismatch: size=1, data=2'),
    (make_response(crc=[0, 0]), 'CRC mismatch'),
    (make_response(code=123), 'received unknown response code: 123'),  # type: ignore
])
def test_reset_protocol_errors(
    picoslave: PicoSlave,
    outep: MagicMock,
    inep: MagicMock,
    response: bytes,
    error_msg: str,
) -> None:
    inep.read.return_value = response
    with pytest.raises(ProtocolError) as exc_info:
        picoslave.reset()
    assert error_msg in str(exc_info.value)
    outep.write.assert_called_once_with(make_command(cmd=CMD.RESET))


@pytest.mark.parametrize('values', [
    ['a', 'b', 'c', 'd'],
    ['', '', '', ''],
    ['serial', 'firmware', 'protocol', 'interfaces'],
])
def test_info(
    picoslave: PicoSlave,
    outep: MagicMock,
    inep: MagicMock,
    values: List[str],
) -> None:
    inep.read.return_value = make_response(data=';'.join(values).encode())
    info = picoslave.info()
    outep.write.assert_called_once_with(make_command(cmd=CMD.INFO))
    assert info['serial'] == values[0]
    assert info['firmware'] == values[1]
    assert info['protocol'] == values[2]
    assert info['interfaces'] == values[3]


@pytest.mark.parametrize('response, error_msg', [
    (make_response(code=CODE.INVALID_INTERFACE), 'received error code 4 (INVALID_INTERFACE)'),
    (make_response(code=CODE.INVALID_REQUEST), 'received error code 3 (INVALID_REQUEST)'),
    (make_response(data=[0xAB, 0xCD]), "failed to decode INFO response: b'\\xab\\xcd'"),
    (make_response(header=[0, 0, 0, 0]), 'received wrong number of bytes'),
    (make_response(packet=[0, 0]), 'invalid received packet size'),
    (make_response(packet=[0, 1, 0, 0xaa, 0xbb]), 'received packet size mismatch: size=1, data=2'),
    (make_response(crc=[0, 0]), 'CRC mismatch'),
    (make_response(code=123), 'received unknown response code: 123'),  # type: ignore
    (make_response(), 'received no data'),
    (make_response(data=';'.encode()), 'received unexpected number of INFO segments'),
    (make_response(data='a;b;c'.encode()), 'received unexpected number of INFO segments'),
    (make_response(data='a;b;c;d;e'.encode()), 'received unexpected number of INFO segments'),
    (make_response(data='a,b,c,d'.encode()), 'received unexpected number of INFO segments'),
])
def test_info_protocol_errors(
    picoslave: PicoSlave,
    outep: MagicMock,
    inep: MagicMock,
    response: bytes,
    error_msg: str,
) -> None:
    inep.read.return_value = response
    with pytest.raises(ProtocolError) as exc_info:
        picoslave.info()
    assert error_msg in str(exc_info.value)
    outep.write.assert_called_once_with(make_command(cmd=CMD.INFO))


@pytest.mark.parametrize('lib_version, device_version, error', [
    ('x.y', None, AssertionError),
    ('1.2', '1.2', None),
    ('0.0', '0.0', None),
    ('10.10', '10.10', None),
    ('0.1', '00.0001', None),
    (None, '1', ProtocolError),
    (None, '1.x', ProtocolError),
    (None, 'x.1', ProtocolError),
    (None, '1..1', ProtocolError),
    ('0.8', '0.9', None),
    ('0.0', '0.9', None),
    ('1.1', '1.2', None),
    ('0.9', '0.8', ProtocolVersionWarning),
    ('0.9', '0.1', ProtocolVersionWarning),
    ('0.9', '1.0', ProtocolVersionError),
    ('1.0', '0.9', ProtocolVersionError),
])
def test_check_protocol(
    picoslave: PicoSlave,
    inep: MagicMock,
    lib_version: str,
    device_version: str,
    error: Optional[type],
) -> None:
    with patch('picoslave.picoslave.PicoSlave.Protocol.VERSION', lib_version or '0.0'):
        inep.read.return_value = make_response(data=f"a;b;{device_version};d".encode())
        if error:
            with pytest.raises(error):
                picoslave.check_protocol()
        else:
            picoslave.check_protocol()


@pytest.mark.parametrize('iface', [0, 1, 2, 3])
def test_clear(picoslave: PicoSlave, outep: MagicMock, inep: MagicMock, iface: int) -> None:
    inep.read.return_value = make_response()
    picoslave.clear(iface=iface)
    outep.write.assert_called_once_with(make_command(cmd=CMD.CLEAR, iface=iface))


@pytest.mark.parametrize('response, error', [
    (make_response(code=CODE.INVALID_INTERFACE), 'received error code 4 (INVALID_INTERFACE)'),
    (make_response(code=CODE.INVALID_REQUEST), 'received error code 3 (INVALID_REQUEST)'),
    (make_response(data=[0xAB, 0xCD]), 'CLEAR received an unexpected response: AB CD'),
    (make_response(header=[0, 0, 0, 0]), 'received wrong number of bytes'),
    (make_response(packet=[0, 0]), 'invalid received packet size'),
    (make_response(packet=[0, 1, 0, 0xaa, 0xbb]), 'received packet size mismatch: size=1, data=2'),
    (make_response(crc=[0, 0]), 'CRC mismatch'),
    (make_response(code=123), 'received unknown response code: 123'),  # type: ignore
])
def test_clear_errors(picoslave: PicoSlave, inep: MagicMock, response: bytes, error: str) -> None:
    inep.read.return_value = response
    with pytest.raises(ProtocolError) as exc_info:
        picoslave.clear(iface=0)
    assert error in str(exc_info.value)


@pytest.mark.parametrize('iface', [0, 1, 2, 3])
@pytest.mark.parametrize('slave_address', [1, 2, 0x7C, 0xFF])
@pytest.mark.parametrize('mem_size', [1, 256, 512])
@pytest.mark.parametrize('mem_width', [1, 2, 4])
def test_config_slave(
    picoslave: PicoSlave,
    outep: MagicMock,
    inep: MagicMock,
    iface: int,
    slave_address: int,
    mem_size: int,
    mem_width: int,
) -> None:
    inep.read.return_value = make_response()
    picoslave.config(
        iface=iface, slave_address=slave_address, mem_size=mem_size, mem_width=mem_width
    )
    outep.write.assert_called_once_with(
        make_command(
            cmd=CMD.CONFIG,
            iface=iface,
            addr=slave_address,
            data=struct.pack('<HH', mem_size, mem_width),
        )
    )


@pytest.mark.parametrize('iface', [0, 1, 2, 3])
@pytest.mark.parametrize('block_scl', [True, False])
@pytest.mark.parametrize('block_sda', [True, False])
def test_config_blocker(
    picoslave: PicoSlave,
    outep: MagicMock,
    inep: MagicMock,
    iface: int,
    block_scl: bool,
    block_sda: bool,
) -> None:
    inep.read.return_value = make_response()
    picoslave.config_blocker(iface=iface, scl=block_scl, sda=block_sda)
    outep.write.assert_called_once_with(
        make_command(
            cmd=CMD.CONFIG,
            iface=iface,
            addr=PicoSlave.Protocol.SlaveFunction.BLOCKER,
            data=struct.pack('<BB', int(block_scl), int(block_sda)),
        )
    )


@pytest.mark.parametrize('iface', [0, 1, 2, 3])
def test_config_blocker_legacy(
    picoslave: PicoSlave,
    outep: MagicMock,
    inep: MagicMock,
    iface: int,
) -> None:
    inep.read.return_value = make_response()
    picoslave.config(iface=iface, slave_address=PicoSlave.Protocol.SlaveFunction.BLOCKER)
    outep.write.assert_called_once_with(
        make_command(
            cmd=CMD.CONFIG,
            iface=iface,
            addr=PicoSlave.Protocol.SlaveFunction.BLOCKER,
            data=struct.pack('<BB', int(True), int(True)),
        )
    )


@pytest.mark.parametrize('response, error', [
    (make_response(code=CODE.INVALID_INTERFACE), 'received error code 4 (INVALID_INTERFACE)'),
    (make_response(code=CODE.INVALID_REQUEST), 'received error code 3 (INVALID_REQUEST)'),
    (make_response(data=[0xAB, 0xCD]), 'CONFIG received an unexpected response: AB CD'),
    (make_response(header=[0, 0, 0, 0]), 'received wrong number of bytes'),
    (make_response(packet=[0, 0]), 'invalid received packet size'),
    (make_response(packet=[0, 1, 0, 0xaa, 0xbb]), 'received packet size mismatch: size=1, data=2'),
    (make_response(crc=[0, 0]), 'CRC mismatch'),
    (make_response(code=123), 'received unknown response code: 123'),  # type: ignore
])
def test_config_errors(picoslave: PicoSlave, inep: MagicMock, response: bytes, error: str) -> None:
    inep.read.return_value = response
    with pytest.raises(ProtocolError) as exc_info:
        picoslave.config(iface=0, slave_address=0, mem_size=1)
    assert error in str(exc_info.value)


def test_close(picoslave: PicoSlave, device: MagicMock) -> None:
    picoslave.close()
    device.reset.assert_called_once()


def test_close_error(picoslave: PicoSlave, device: MagicMock) -> None:
    device.reset.side_effect = USBError('reset error')
    with pytest.raises(UsbError) as exc_info:
        picoslave.close()
    assert 'Failed to close the device' in str(exc_info.value)
    assert 'reset error' in str(exc_info.value)


@pytest.mark.parametrize('iface', [0, 1, 2, 3])
@pytest.mark.parametrize('addr', [0, 1, 2**16 - 1])
@pytest.mark.parametrize('data', [bytes([1, 2, 3, 4]), bytes(1000)])
def test_write(
    picoslave: PicoSlave,
    inep: MagicMock,
    outep: MagicMock,
    iface: int,
    addr: int,
    data: bytes,
) -> None:
    inep.read.return_value = make_response()
    picoslave.write(iface=iface, mem_addr=addr, data=data)
    outep.write.assert_called_once_with(
        make_command(cmd=CMD.WRITE, iface=iface, addr=addr, data=data)
    )


@pytest.mark.parametrize('response, error', [
    (make_response(code=CODE.INVALID_INTERFACE), 'received error code 4 (INVALID_INTERFACE)'),
    (make_response(code=CODE.INVALID_REQUEST), 'received error code 3 (INVALID_REQUEST)'),
    (make_response(data=[0xAB, 0xCD]), 'WRITE received an unexpected response: AB CD'),
    (make_response(header=[0, 0, 0, 0]), 'received wrong number of bytes'),
    (make_response(packet=[0, 0]), 'invalid received packet size'),
    (make_response(packet=[0, 1, 0, 0xaa, 0xbb]), 'received packet size mismatch: size=1, data=2'),
    (make_response(crc=[0, 0]), 'CRC mismatch'),
    (make_response(code=123), 'received unknown response code: 123'),  # type: ignore
])
def test_write_errors(picoslave: PicoSlave, inep: MagicMock, response: bytes, error: str) -> None:
    inep.read.return_value = response
    with pytest.raises(ProtocolError) as exc_info:
        picoslave.write(iface=0, mem_addr=0, data=bytes(10))
    assert error in str(exc_info.value)


@pytest.mark.parametrize('iface', [0, 1, 2, 3])
@pytest.mark.parametrize('addr', [0, 1, 2**16 - 1])
@pytest.mark.parametrize('data', [bytes([1, 2, 3, 4]), bytes(1000)])
def test_read(
    picoslave: PicoSlave,
    inep: MagicMock,
    outep: MagicMock,
    iface: int,
    addr: int,
    data: bytes,
) -> None:
    inep.read.return_value = make_response(data=data)
    size = len(data)
    rx_data = picoslave.read(iface=iface, mem_addr=addr, size=size)
    outep.write.assert_called_once_with(
        make_command(cmd=CMD.READ, iface=iface, addr=addr, size=size)
    )
    assert rx_data == data


@pytest.mark.parametrize('response, error', [
    (make_response(code=CODE.INVALID_INTERFACE), 'received error code 4 (INVALID_INTERFACE)'),
    (make_response(code=CODE.INVALID_REQUEST), 'received error code 3 (INVALID_REQUEST)'),
    (make_response(data=[]), 'received no data'),
    (make_response(header=[0, 0, 0, 0]), 'received wrong number of bytes'),
    (make_response(packet=[0, 0]), 'invalid received packet size'),
    (make_response(packet=[0, 1, 0, 0xaa, 0xbb]), 'received packet size mismatch: size=1, data=2'),
    (make_response(crc=[0, 0]), 'CRC mismatch'),
    (make_response(code=123), 'received unknown response code: 123'),  # type: ignore
])
def test_read_errors(picoslave: PicoSlave, inep: MagicMock, response: bytes, error: str) -> None:
    inep.read.return_value = response
    with pytest.raises(ProtocolError) as exc_info:
        picoslave.read(iface=0, mem_addr=0, size=1)
    assert error in str(exc_info.value)


@pytest.mark.parametrize('iface', [0, 1, 2, 3])
@pytest.mark.parametrize('addr', [0, 1, 256])
@pytest.mark.parametrize('stats', [
    [(1, 1)],
    [(1, 1)],
])
def test_statistics(
    picoslave: PicoSlave,
    inep: MagicMock,
    outep: MagicMock,
    iface: int,
    addr: int,
    stats: List[Tuple[int, int]],
) -> None:
    data = b''.join([struct.pack('<I', stat[0]) + struct.pack('<I', stat[1]) for stat in stats])
    inep.read.return_value = make_response(data=data)
    statistics = picoslave.statistics(iface=iface, mem_addr=addr, size=len(stats))

    for i in range(len(stats)):
        assert statistics[addr + i][0] == stats[i][0]
        assert statistics[addr + i][1] == stats[i][1]

    outep.write.assert_called_once_with(
        make_command(cmd=CMD.STAT, iface=iface, addr=addr, size=len(stats))
    )


@pytest.mark.parametrize('response, error', [
    (make_response(code=CODE.INVALID_INTERFACE), 'received error code 4 (INVALID_INTERFACE)'),
    (make_response(code=CODE.INVALID_REQUEST), 'received error code 3 (INVALID_REQUEST)'),
    (make_response(data=[]), 'received no data'),
    (make_response(data=[0, 0, 0, 0]), 'received data size is no multiple of 8 (size=4)'),
    (make_response(header=[0, 0, 0, 0]), 'received wrong number of bytes'),
    (make_response(packet=[0, 0]), 'invalid received packet size'),
    (make_response(packet=[0, 1, 0, 0xaa, 0xbb]), 'received packet size mismatch: size=1, data=2'),
    (make_response(crc=[0, 0]), 'CRC mismatch'),
    (make_response(code=123), 'received unknown response code: 123'),  # type: ignore
])
def test_statistics_errors(
    picoslave: PicoSlave, inep: MagicMock, response: bytes, error: str
) -> None:
    inep.read.return_value = response
    with pytest.raises(ProtocolError) as exc_info:
        picoslave.statistics(iface=0, mem_addr=0, size=1)
    assert error in str(exc_info.value)
