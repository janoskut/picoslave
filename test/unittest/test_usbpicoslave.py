from typing import Any, List

from unittest.mock import patch, MagicMock

import pytest

from usb.core import USBTimeoutError, USBError

from .fixtures import DeviceMock
from .fixtures import *  # noqa

from picoslave.picoslave import bytestr, DeviceNotFoundError, ProtocolError, UsbError, UsbPicoSlave


@patch('usb.core.find')
@pytest.mark.parametrize('devices', [
    None,
    [],
    ['a', 'b', 'c'],  # non-devices
])
def test_scan_no_devices(mock_find: MagicMock, devices: List[List[Any]]) -> None:

    mock_find.return_value = devices

    a = UsbPicoSlave.scan()

    mock_find.assert_called_once_with(
        idVendor=UsbPicoSlave.ID_VENDOR,
        idProduct=UsbPicoSlave.ID_PRODUCT,
        find_all=True,
    )
    assert a is not None
    assert len(a) == 0


@patch('usb.core.find')
def test_scan_bad_and_good_devices(mock_find: MagicMock) -> None:

    devices = [
        object(),  # bad device, will raise error
        DeviceMock('abc')
    ]

    mock_find.return_value = devices

    result = UsbPicoSlave.scan()

    assert len(result) == 1
    assert result[0].serial_number == 'abc'


@patch('usb.core.find')
def test_scan_multiple(mock_find: MagicMock) -> None:

    devices = [
        DeviceMock('abc'),
        DeviceMock('def'),
    ]

    mock_find.return_value = devices

    result = UsbPicoSlave.scan()

    assert len(result) == 2
    assert result[0].serial_number == 'abc'
    assert result[1].serial_number == 'def'


def test_find_bad_filter() -> None:
    with pytest.raises(AssertionError):
        UsbPicoSlave.find(index=1, serial='abc')


@patch('usb.util.find_descriptor')
@patch('usb.core.find')
@pytest.mark.parametrize('active_config, descriptors', [
    (None, []),
    (object(), [None, None]),
    (object(), [object(), None]),
    (object(), [object(), object(), None]),
])
def test_find_bad_device(find: MagicMock, find_descriptor: MagicMock,
                         active_config: Any, descriptors: List[Any]) -> None:
    device = DeviceMock('abc')
    device.active_config = active_config
    find.return_value = [device]
    find_descriptor.side_effect = descriptors
    with pytest.raises(UsbError):
        UsbPicoSlave.find()


@patch('usb.core.find', return_value=[])
def test_find_no_filter_no_device(_: MagicMock) -> None:
    with pytest.raises(DeviceNotFoundError):
        UsbPicoSlave.find()


@patch('usb.core.find')
def test_find_no_filter_multiple_devices(find: MagicMock) -> None:
    find.return_value = [
        DeviceMock('abc'),
        DeviceMock('def'),
    ]
    with pytest.raises(DeviceNotFoundError):
        UsbPicoSlave.find()


@patch('usb.util.find_descriptor')
@patch('usb.core.find')
def test_find_no_filter_single_device(find: MagicMock, find_descriptor: MagicMock) -> None:

    device = DeviceMock('abc')

    find.return_value = [device]

    find_descriptor.side_effect = [
        object(),  # intf
        object(),  # outep
        object(),  # inep
    ]

    usb_picoslave = UsbPicoSlave.find()
    assert usb_picoslave is not None
    assert usb_picoslave._device == device


@patch('usb.util.find_descriptor', side_effect=[object(), object(), object()])
@patch('usb.core.find', return_value=[
    DeviceMock('abc'),
    DeviceMock('def'),
    DeviceMock('ghi')
])
@pytest.mark.parametrize('index, expected', [
    (0, 'abc'),
    (1, 'def'),
    (2, 'ghi'),
])
def test_find_by_index(find: MagicMock, find_desc: MagicMock, index: int, expected: str) -> None:
    usb_picoslave = UsbPicoSlave.find(index=index)
    assert usb_picoslave is not None
    assert usb_picoslave._device.serial_number == expected


@patch('usb.util.find_descriptor', side_effect=[object(), object(), object()])
@patch('usb.core.find')
@pytest.mark.parametrize('index, devices', [
    (0, []),
    (1, []),
    (1, [DeviceMock('abc')]),
    (2, [DeviceMock('abc'), DeviceMock('def')]),
])
def test_find_by_index_not_found(find: MagicMock, _: MagicMock, index: int,
                                 devices: List[DeviceMock]) -> None:
    find.return_value = devices
    with pytest.raises(DeviceNotFoundError):
        UsbPicoSlave.find(index=index)


@patch('usb.util.find_descriptor', side_effect=[object(), object(), object()])
@patch('usb.core.find')
@pytest.mark.parametrize('serial, devices', [
    ('abc', []),
    ('def', [DeviceMock('abc')]),
    ('def', [DeviceMock('abc'), DeviceMock('abc')]),
    ('ghi', [DeviceMock('abc'), DeviceMock('def')]),
])
def test_find_by_serial_not_found(find: MagicMock, _: MagicMock, serial: str,
                                  devices: List[DeviceMock]) -> None:
    find.return_value = devices
    with pytest.raises(DeviceNotFoundError):
        UsbPicoSlave.find(serial=serial)


@patch('usb.util.find_descriptor', side_effect=[object(), object(), object()])
@patch('usb.core.find')
@pytest.mark.parametrize('serial, devices', [
    ('abc', [DeviceMock('abc'), DeviceMock('abc')]),
    ('def', [DeviceMock('abc'), DeviceMock('def'), DeviceMock('def')]),
])
def test_find_by_serial_multiple_matches(find: MagicMock, _: MagicMock, serial: str,
                                         devices: List[DeviceMock]) -> None:
    find.return_value = devices
    with pytest.raises(DeviceNotFoundError):
        UsbPicoSlave.find(serial=serial)


@patch('usb.util.find_descriptor', side_effect=[object(), object(), object()])
@patch('usb.core.find')
@pytest.mark.parametrize('serial, devices', [
    ('def', [DeviceMock('abc'), DeviceMock('abc'), DeviceMock('def')]),
    ('abc', [DeviceMock('abc'), DeviceMock('def'), DeviceMock('def')]),
])
def test_find_by_serial_single_match(find: MagicMock, _: MagicMock, serial: str,
                                     devices: List[DeviceMock]) -> None:
    find.return_value = devices
    usb_picoslave = UsbPicoSlave.find(serial=serial)
    assert usb_picoslave is not None
    assert usb_picoslave._device.serial_number == serial


def test_close(device: DeviceMock, usb_picoslave: UsbPicoSlave) -> None:
    usb_picoslave.close()
    device.reset.assert_called_once()


def test_close_error(device: DeviceMock, usb_picoslave: UsbPicoSlave) -> None:
    device.reset.side_effect = USBError('reset error')
    with pytest.raises(UsbError) as exc_info:
        usb_picoslave.close()
    assert 'Failed to close the device' in str(exc_info.value)
    assert 'reset error' in str(exc_info.value)


def test_write(usb_picoslave: UsbPicoSlave, outep: MagicMock) -> None:
    usb_picoslave.write(b'')
    outep.write.assert_called_with(b'\0\0\0\0')
    usb_picoslave.write(b'a')
    outep.write.assert_called_with(b'\1\0\0\0a')
    usb_picoslave.write(b'abcdef')
    outep.write.assert_called_with(b'\6\0\0\0abcdef')


def test_write_exception(usb_picoslave: UsbPicoSlave, outep: MagicMock, inep: MagicMock) -> None:
    usb_exception = USBTimeoutError('nasty')
    outep.write.side_effect = usb_exception
    inep.read.side_effect = usb_exception

    with pytest.raises(UsbError) as excinfo:
        usb_picoslave.write(b'thiswillfail')

    assert excinfo.value.__cause__ is usb_exception


@pytest.mark.parametrize('read, expected', [
    (b'\0\0\0\0', b''),
    (b'\1\0\0\0a', b'a'),
    (b'\6\0\0\0foobar', b'foobar'),
])
def test_read(usb_picoslave: UsbPicoSlave, inep: MagicMock, read: bytes, expected: bytes) -> None:
    inep.read.side_effect = [read]
    result = usb_picoslave.read()
    assert result == expected
    inep.read.assert_called_once_with(8192)


def test_read_error(usb_picoslave: UsbPicoSlave, inep: MagicMock) -> None:
    usb_exception = USBTimeoutError('nasty')
    inep.read = MagicMock()
    inep.read.side_effect = usb_exception

    with pytest.raises(UsbError) as excinfo:
        usb_picoslave.read()

    assert excinfo.value.__cause__ is usb_exception


@pytest.mark.parametrize('read', [
    b'\1\0\0\0',
    b'\0\0\0\0a',
    b'\0\0\0\1a',
    b'\5\0\0\0foobar',
    b'\7\0\0\0foobar',
])
def test_read_protocol_error(usb_picoslave: UsbPicoSlave, inep: MagicMock, read: bytes) -> None:
    inep.read.side_effect = [read]

    with pytest.raises(ProtocolError) as excinfo:
        usb_picoslave.read()

    assert "wrong number of bytes" in str(excinfo.value)


def test_bytestr() -> None:

    with pytest.raises(AssertionError):
        bytestr(None)  # type: ignore

    assert bytestr(b'') == ''
    assert bytestr(bytes([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])) == \
        '00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F'
    assert bytestr(bytes([0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff])) == 'AA BB CC DD EE FF'
    assert bytestr(b'foobar') == '66 6F 6F 62 61 72'
