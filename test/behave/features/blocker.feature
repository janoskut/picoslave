Feature: Slave Configuration

    The blocker feature allows to tie the SDA or/and the SCL signal to GND, blocking I2C
    communication.

    Testing this feature is tricky, as it relies on specific error propagation and handling of
    the underlaying I2C driver. When the SDA signal is stuck low, a bus scan would find slaves on
    all addresses. Somehow our bus scan implementation seems to write 0 data into the PicoSlave.

    On the other hand, a stuck SCL line would not find any slave on a bus scan, but that would take
    forever to fail. Also, the driver seems to apply some clock stretching recovery, which will
    also fail some ~3-5 reads even after the stuck SCL has been released. To solve this, the
    read retries have been increased from 3 to 10 just for this test.


    Background:
        Given slave "1" is configured for address "0x16"
        And I write "16" random bytes to USB slave "1" at address "0x00"
        And I write "abcdef" to USB slave "1" at address "0xF0"


    Scenario: SDA signal blocked

        Given slave "0" is configured for address "0x00"
        Then a bus scan finds a slave on address "0x16"
        And reading I2C slave "0x16" at address "0x00" with length "16" gives "random_bytes"

        Given slave "0" is configured as blocker for signals "sda"
        # with a blocked SDA signal, all data reads as 0
        Then reading I2C slave "0x16" at address "0x00" with length "16" only contains "00"

        # Release SDA blockage
        Given slave "0" is configured for address "0x00"
        # with the low SDA signal, our slave has previously mis-interpreted reads as "write 0" commands
        Then reading I2C slave "0x16" at address "0x00" with length "16" only contains "00"
        # But the other range remained untouched
        Then reading I2C slave "0x16" at address "0xF0" with length "3" gives "abcdef"


    Scenario: SCL signal blocked

        Given slave "0" is configured for address "0x00"
        Then a bus scan finds a slave on address "0x16"
        And reading I2C slave "0x16" at address "0x00" with length "16" gives "random_bytes"

        Given slave "0" is configured as blocker for signals "scl"
        When I read "1" bytes from I2C slave "0x16" at address "0x00"
        Then an "TimeoutError" was thrown that contained "Connection timed out"

        # Release SDA blockage
        Given slave "0" is configured for address "0x00"
        Then reading I2C slave "0x16" at address "0x00" with length "16" gives "random_bytes"
