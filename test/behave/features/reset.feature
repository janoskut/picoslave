Feature: Reset command

    The RESET command is an important command to un-initialize or un-configure a PicoSlave.

    Resetting the PicoSlave performs a complete reset of the microcontroller, causing all slave
    configuration to be reset and all memory to be erased.


    Scenario: Reset removes configured slaves
        Given slave "0" is configured for address "0x16"
        And slave "1" is configured for address "0x23"

        Then a bus scan finds a slave on address "0x16"
        And a bus scan finds a slave on address "0x23"

        When I reset the PicoSlave device
        Then a bus scan finds no slave


    Scenario: Reset clears memory
        Given slave "0" is configured for address "0x16"
        And slave "1" is configured for address "0x23"

        When I write "1024" random bytes to USB slave "0" at address "0x00"
        When I write "1024" random bytes to USB slave "1" at address "0x00"

        When I reset the PicoSlave device

        Then reading USB slave "0" at address "0x00" with length "1024" only contains "00"
        Then reading USB slave "1" at address "0x00" with length "1024" only contains "00"
