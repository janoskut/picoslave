import random
import os
import time
import traceback

from behave.runner import Context

from impl.i2c import I2c
from impl.usb import UsbHub
from impl.device import PicoSlaveDevice
from impl import error_capture


def before_all(context: Context) -> None:
    try:
        if 'seed' in context.config.userdata:
            seed = context.config.userdata['seed']
            print(f'using seed from userdata: {seed}')
        else:
            seed = os.getrandom(16)
            seed = "".join(f"{b:02x}" for b in seed)
            print(f'initializing random seed: {seed}')
        context.seed = seed
        random.seed(seed)

        context.config.setup_logging()

        context.i2c = I2c(bus_id=1)

        usb_hub = UsbHub(hub_location='2')
        context.device = PicoSlaveDevice(usb_hub)

        if 'reset-device' in context.config.userdata:
            print(f'resetting PicoSlave ...: {seed}')
            context.device.reset()
            time.sleep(1)
        else:
            context.device.plugin()

    except Exception:
        traceback.print_exc()
        raise


def after_all(context: Context) -> None:

    try:
        context.device.close()
    except Exception as exc:
        print(f'failed to close PicoSlave: {str(exc)}')

    if context.failed:
        print(f'Behave failed. Random seed was: {context.seed}')
        print(f'Run with -Dseed={context.seed} to repeat')


def before_step(context: Context, step: str) -> None:
    context.step = step


def before_scenario(context: Context, scenario: str) -> None:
    error_capture.init(context)


def after_scenario(context: Context, scenario: str) -> None:
    error_capture.teardown(context)
    # de-initialize both slaves
    context.device.picoslave.config(0, 0x00)
    context.device.picoslave.config(1, 0x00)
