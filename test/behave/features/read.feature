Feature: Read from PicoSlave memory

    Background:
        Given slave "0" is configured for address "0x16"
        And slave "0" is cleared


    Scenario Outline: Reading single bytes from I2C
        When I write "<data>" to I2C slave "0x16" at address "<addr>"
        Then reading USB slave "0" at address "<addr>" with length "1" gives "<data>"
        And reading I2C slave "0x16" at address "<addr>" with length "1" gives "<data>"
        Examples:
            | addr  | data |
            | 0x00  | AB   |
            | 0x01  | CD   |
            | 0xFF  | EF   |
            | 0xFF  | FF   |


    Scenario Outline: Reading multiple single bytes from I2C
        When I write "<value1>" to I2C slave "0x16" at address "<addr1>"
        And I write "<value2>" to I2C slave "0x16" at address "<addr2>"
        Then reading USB slave "0" at address "<addr1>" with length "2" gives "<expected>"
        And reading I2C slave "0x16" at address "<addr1>" with length "2" gives "<expected>"
        Examples:
            | addr1 | addr2 | value1 | value2 | expected |
            | 0x00  | 0x01  | DE     | AD     | DEAD     |
            | 0xDE  | 0xDF  | BE     | EF     | BEEF     |
            | 0xFE  | 0xFF  | FF     | FF     | FFFF     |


    Scenario Outline: Reading multiple bytes
        When I write "<value>" to I2C slave "0x16" at address "<addr>"
        Then reading USB slave "0" at address "<addr>" with length "<length>" gives "<value>"
        And reading I2C slave "0x16" at address "<addr>" with length "<length>" gives "<value>"
        Examples:
            | addr | value    | length |
            | 0x00 | abcdef   | 3      |
            | 0x00 | ffffffff | 4      |
            | 0xAB | deadbeef | 4      |


    Scenario Outline: Reading many bytes
        When I write "<num>" random bytes to I2C slave "0x16" at address "<addr>"
        Then reading USB slave "0" at address "<addr>" with length "<num>" gives "random_bytes"
        And reading I2C slave "0x16" at address "<addr>" with length "<num>" gives "random_bytes"
        Examples:
            | num | addr |
            | 1   | 0xAB |
            | 10  | 0xAB |
            | 32  | 0x80 |
            | 256 | 0x00 |


    Scenario Outline: Reading out of bounds gives overflow error
        When I read "<num>" bytes from USB slave "0" at address "<addr>"
        Then an "ProtocolError" was thrown that contained "MEMORY_ERROR"
        Examples:
            | num  | addr |
            | 2    | 0x3FF |
            | 4    | 0x3FD |
            | 16   | 0x3F1 |
            | 255  | 0x302 |
            | 257  | 0x300 |
            | 1000 | 25    |
