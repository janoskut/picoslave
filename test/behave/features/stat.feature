Feature: Read/Write Statisticts

    When I2C read or write transactions are done on a PicoSlave, PicoSlave counts each of the
    read and each of the write access to every memory address. The counted values can be retrieved
    through the USB interface to give an overview, which memory addresses have been written to
    and which memory addresses have been read from.


    Background: One slave is configured
        Given slave "0" is configured for address "0x16"
        And slave "0" is cleared


    Scenario Outline: Simple writing to I2C gives write statistics
        When I write "<value>" to I2C slave "0x16" at address "<addr>"
        And I read "<N>" statistics entries from USB slave "0" at address "<addr>"
        Then statistics contains "<expected>"
        Examples:
            | value   | addr | N   | expected                       |
            | ab      | 0x00 | 1   | { 0x00: (0, 1)}                |
            | ab      | 0xFF | 1   | { 0xFF: (0, 1)}                |
            | ab      | 0x00 | 256 | { 0x00: (0, 1)}                |
            | abcd    | 0x00 | 2   | { 0x00: (0, 1), 0x01: (0, 1)}  |
            | abcd    | 0x00 | 256 | { 0x00: (0, 1), 0x01: (0, 1)}  |


    Scenario: Multiple writing to I2C gives write statistics
        When I write "abcd" to I2C slave "0x16" at address "0x01"
        And I write "ef" to I2C slave "0x16" at address "0x02"
        And I read "4" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0:(0,0), 1:(0,1), 2:(0,2), 3:(0,0)}"


    Scenario Outline: Simple reading to I2C gives read statistics
        When I read "<N>" bytes from I2C slave "0x16" at address "<addr>"
        And I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "<expected>"
        Examples:
            | addr | N | expected                       |
            | 0x00 | 1 | { 0x00: (1, 0)}                |
            | 0xFF | 1 | { 0xFF: (1, 0)}                |
            | 0x00 | 2 | { 0x00: (1, 0), 0x01: (1, 0)}  |


    Scenario: Multiple reading from I2C gives read statistics
        When I read "2" bytes from I2C slave "0x16" at address "0x01"
        And I read "1" bytes from I2C slave "0x16" at address "0x02"
        And I read "4" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0:(0,0), 1:(1,0), 2:(2,0), 3:(0,0)}"


    Scenario: Read and Write statistics
        When I read "2" bytes from I2C slave "0x16" at address "0x01"
        And I write "abcdef" to I2C slave "0x16" at address "0x00"
        And I read "1" bytes from I2C slave "0x16" at address "0x02"
        And I write "1234" to I2C slave "0x16" at address "0x01"
        And I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{ 0x00: (0,1), 0x01: (1,2), 0x02: (2,2), 0x03: (0,0) }"


    Scenario Outline: Statistic read overflow
        When I read "<size>" statistics entries from USB slave "0" at address "<addr>"
        Then an "<error>" was thrown that contained "<content>"
        Examples:
            | size  | addr | error         | content      |
            | 257   | 0x00 | ProtocolError | MEMORY_ERROR |
            | 256   | 0x01 | ProtocolError | MEMORY_ERROR |
            | 2     | 0xFF | ProtocolError | MEMORY_ERROR |
            | 65535 | 0x00 | ProtocolError | INVALID_SIZE |


    Scenario: After re-configure of slave, the statistics are cleared
        When I read "2" bytes from I2C slave "0x16" at address "0xab"
        And I write "abcdef" to I2C slave "0x16" at address "0xab"
        And I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0xab:(1,1), 0xac:(1,1), 0xad:(0,1)}"

        When I configure USB slave "0" for address "0x16"
        And I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics are empty
        But reading USB slave "0" at address "0xab" with length "3" gives "abcdef"
