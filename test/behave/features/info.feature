Feature: The info command retrieves device information

    The format of the returnes string is like this:
    E660C062138D532C;v0.0-64-g38a11b8*;1.0;2


    Scenario: Reading device info
        When I read the device info
        Then device info contains
            | key        | pattern        |
            | serial     | ^[0-9A-Z]{16}$ |
            | firmware   | <semver>       |
            | protocol   | 1.1            |
            | interfaces | 2              |
