import ast
import random
import time
from typing import cast, Any, Dict

from behave import given, when, then, step
from behave.runner import Context

import semver

from impl import error_capture
from impl import util
from impl.device import PicoSlaveDevice

from hamcrest import assert_that, is_, only_contains, has_key, matches_regexp

from picoslave.picoslave import PicoSlave, UsbPicoSlave


####################################################################################################
# HELPERS
####################################################################################################

def device(context: Context) -> PicoSlaveDevice:
    """Access helper to provide a typed context around untyped context attributes."""
    return cast(PicoSlaveDevice, context.device)


def picoslave(context: Context) -> PicoSlave:
    """Access helper to provide a typed context around untyped context attributes."""
    return device(context).picoslave


####################################################################################################
# GIVEN
####################################################################################################

@step('I wait for {seconds:g} seconds')
def step_impl(context: Context, seconds: int) -> None:
    print(f'sleeping for {seconds} seconds ...')
    time.sleep(seconds)


@given('slave "{id:d}" is configured for address "{addr:x}"')  # type: ignore
def step_impl(context: Context, id: int, addr: int) -> None:
    picoslave(context).config(iface=id, slave_address=addr)


@given('slave "{id:d}" is configured as blocker for signals "{signals_str}"')  # type: ignore
def step_impl(context: Context, id: int, signals_str: str) -> None:
    valid_signals = ['scl', 'sda']
    signals = set([sig.strip() for sig in signals_str.split(',')])
    invalid_signals = [sig for sig in signals if sig not in valid_signals]
    assert not invalid_signals, \
        f'input contains invalid signals: {invalid_signals}. Valid signals are: {valid_signals}'
    block_scl = 'scl' in signals
    block_sda = 'sda' in signals
    picoslave(context).config_blocker(iface=id, scl=block_scl, sda=block_sda)


@given('slave "{id:d}" is configured for address "{addr:x}", size "{size:d}" '  # type: ignore
       'and width "{width:d}"')
def step_impl(context: Context, id: int, addr: int, size: int, width: int):
    picoslave(context).config(iface=id, slave_address=addr, mem_size=size, mem_width=width)


@given('slave "{id:d}" is cleared')  # type: ignore
def step_impl(context: Context, id: int) -> None:
    picoslave(context).clear(id, 0x00)


@given('the PicoSlave device is "{action}"')  # type: ignore
def step_impl(context: Context, action: str) -> None:
    action_map = {
        'plugged-in': device(context).plugin,
        'unplugged': device(context).unplug,
        'reset': device(context).reset,
    }
    assert action in action_map, f'not allowed action: {action}'
    action_map[action]()


@given('I know the serial number of the PicoSlave device')  # type: ignore
def step_impl(context: Context) -> None:
    context.known_serial = picoslave(context).info()['serial']


####################################################################################################
# WHEN
####################################################################################################

@when('I configure USB slave "{id:d}" for address "{addr:x}"')  # type: ignore
@error_capture.capture
def step_impl(context: Context, id: int, addr: int) -> None:  # noqa F811
    picoslave(context).config(id, addr)


@when('I configure USB slave "{id:d}" for address "{addr:x}" '  # type: ignore
      'with memory size "{size:d}"')
@error_capture.capture
def step_impl(context: Context, id: int, addr: int, size: int) -> None:
    picoslave(context).config(id, addr, size)


@step('I write "{data:w}" to USB slave "{id:d}" at address "{addr:x}"')  # type: ignore
@error_capture.capture
def step_impl(context: Context, id: int, addr: int, data: str) -> None:
    data_bytes = bytes.fromhex(data)
    picoslave(context).write(id, addr, data_bytes)


@when('I read "{num:d}" bytes from USB slave "{id:d}" at address "{addr:x}"')  # type: ignore
@error_capture.capture
def step_impl(context: Context, id: int, addr: int, num: int) -> None:
    picoslave(context).read(id, addr, num)


@when('I write "{data:w}" to I2C slave "{slave_addr:x}" at address "{addr:x}"')  # type: ignore
def step_impl(context: Context, slave_addr: int, addr: int, data: str) -> None:
    context.i2c.write(slave_addr, addr, bytes.fromhex(data))


@when('I read "{num:d}" bytes from I2C slave "{slave_addr:x}" '  # type: ignore
      'at address "{addr:x}"')
@error_capture.capture
def step_impl(context: Context, slave_addr: int, addr: int, num: int) -> None:
    context.i2c.read(slave_addr, addr, num)


@step('I write "{num:d}" random bytes to USB slave "{id:d}" at address "{addr:x}"')  # type: ignore
def step_impl(context: Context, id: int, addr: int, num: int) -> None:
    random_bytes = bytes(random.getrandbits(8) for _ in range(num))
    context.random_bytes = random_bytes
    picoslave(context).write(id, addr, random_bytes)


@when('I write "{num:d}" random bytes to I2C slave "{slave_addr:x}" '  # type: ignore
      'at address "{addr:x}"')
def step_impl(context: Context, slave_addr: int, addr: int, num: int) -> None:
    random_bytes = bytes(random.getrandbits(8) for _ in range(num))
    context.random_bytes = random_bytes
    pos = 0
    while num > 0:
        write_len = min(32, num)  # max r/w number for I2C
        context.i2c.write(slave_addr, addr, random_bytes[pos:pos + write_len])
        addr += write_len
        num -= write_len
        pos += write_len


@when('I read "{num:d}" statistics entries from USB slave "{id:d}" '  # type: ignore
      'at address "{addr:x}"')
@error_capture.capture
def step_impl(context: Context, id: int, addr: int, num: int) -> None:
    context.statistics = picoslave(context).statistics(id, addr, num)


@when('I read the device info')  # type: ignore
def step_impl(context: Context) -> None:
    context.device_info = picoslave(context).info()


@when('I reset the PicoSlave device')  # type: ignore
def step_impl(context: Context) -> None:
    picoslave(context).reset()


@when('I scan PicoSlave devices')  # type: ignore
def step_impl(context: Context) -> None:
    context.found_devices = UsbPicoSlave.scan()


@when('I search any PicoSlave device')  # type: ignore
@error_capture.capture
def step_impl(context: Context) -> None:
    context.found_devices = [UsbPicoSlave.find()]


@when('I search a PicoSlave device by serial number "{serial}"')  # type: ignore
@error_capture.capture
def step_impl(context: Context, serial: str) -> None:
    if serial == '<known>':
        assert 'known_serial' in context, "no 'known_serial' in context"
        serial = context.known_serial
    context.found_devices = [UsbPicoSlave.find(serial=serial)]


@when('I search a PicoSlave device at index {index:d}')  # type: ignore
@error_capture.capture
def step_impl(context: Context, index: int) -> None:
    context.found_devices = [UsbPicoSlave.find(index=index)]


####################################################################################################
# THEN
####################################################################################################

@then('a bus scan finds no slave')  # type: ignore
def step_impl(context: Context) -> None:
    slaves = context.i2c.bus_scan()
    assert_that(not slaves, f'expected no slave but found: {slaves}')


@then('a bus scan finds a slave on address "{addr:x}"')  # type: ignore
def step_impl(context: Context, addr: int) -> None:
    addr = addr
    slaves = context.i2c.bus_scan()
    assert_that(addr in slaves, f'{addr:x} was not in {slaves}')


@then('an "{error}" was thrown that contained "{content}"')  # type: ignore
def step_impl(context: Context, error: str, content: str) -> None:
    def expect(exc: Exception):
        return error in str(type(exc)) and content in str(exc)
    assert_that(error_capture.expect(context, expect),
                f'expected error was not raised: error:{error}, content:{content}')


@then('reading I2C slave "{slave_addr:x}" at address "{addr:x}" '  # type: ignore
      'with length "{length:d}" gives "{data:w}"')
def step_impl(context: Context, slave_addr: int, addr: int, length: int, data: str) -> None:
    if data == 'random_bytes':
        expected_result = context.random_bytes
    else:
        expected_result = bytes.fromhex(data)

    def attempt() -> None:
        reading = context.i2c.read(slave_addr, addr, length)
        assert_that(util.bytestr(reading), is_(util.bytestr(expected_result)))

    retries = 10
    assert_that(not util.retry(attempt, retries), f'expected to pass after {retries}, but failed. '
                                                  f'See the logs for details.')


@then('reading USB slave "{id:d}" at address "{addr:x}" '  # type: ignore
      'with length "{length:d}" gives "{data:w}"')
def step_impl(context: Context, id: int, addr: int, length: int, data: str) -> None:
    if data == 'random_bytes':
        expected_result = context.random_bytes
    else:
        expected_result = bytes.fromhex(data)
    reading = picoslave(context).read(id, addr, length)
    assert_that(util.bytestr(reading), is_(util.bytestr(expected_result)))


@then('reading I2C slave "{slave_addr:d}" at address "{addr:x}" '  # type: ignore
      'with length "{length:d}" only contains "{data:w}"')
def step_impl(context: Context, slave_addr: int, addr: int, length: int, data: str) -> None:
    expected_value = bytes.fromhex(data)
    assert len(expected_value) == 1, 'expect exactlt one value'
    reading = context.i2c.read(slave_addr, addr, length)
    assert_that(reading, only_contains(expected_value[0]))


@then('reading USB slave "{id:d}" at address "{addr:x}" '  # type: ignore
      'with length "{length:d}" only contains "{data:w}"')
def step_impl(context: Context, id: int, addr: int, length: int, data: str) -> None:
    expected_value = bytes.fromhex(data)
    assert len(expected_value) == 1, 'expect exactlt one value'
    reading = picoslave(context).read(id, addr, length)
    assert_that(reading, only_contains(expected_value[0]))


@then('statistics contains "{expected_dict}"')  # type: ignore
def step_impl(context: Context, expected_dict: str) -> None:
    expected = ast.literal_eval(expected_dict)
    actual = context.statistics
    for key in expected:
        assert_that(key in actual, f'statistics didnt count addr: 0x{key:02X}')
        assert_that(actual[key], is_(expected[key]))

    def nonzero(stats: Dict[Any, Any]) -> Dict[Any, Any]:
        return {a: stats[a] for a in stats if stats[a] != (0, 0)}
    assert_that(nonzero(actual), is_(nonzero(expected)))


@then('statistics are empty')  # type: ignore
def step_impl(context: Context) -> None:
    statistics: Dict[Any, Any] = context.statistics
    non_zero = [addr for addr in statistics if statistics[addr] != (0, 0)]
    assert_that(len(non_zero), is_(0))


@then('device info contains')  # type: ignore
def step_impl(context: Context) -> None:
    info: Dict[str, Any] = context.device_info

    for row in context.table:
        assert_that(info, has_key(row['key']))

        value = info[row['key']]
        pattern = row['pattern']

        if pattern == '<semver>':
            if value.startswith('v'):  # remove leading 'v'
                value = value[1:]
            if value.endswith('*'):  # remove dirty flag
                value = value[:-1]
            try:
                semver.VersionInfo.parse(value)
            except ValueError as exc:
                assert_that(False, f"'firmware' didn't match: {str(exc)}")
        else:
            assert_that(value, matches_regexp(pattern))


@then('the list of found devices contains {count:d} devices')  # type: ignore
def step_impl(context: Context, count: int):
    assert 'found_devices' in context, "must perform 'scan' or 'find' first"
    assert_that(len(context.found_devices), is_(count))
