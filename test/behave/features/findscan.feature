Feature: Find and Scan function of picoslave

    The find and scan functions help to find devices.


    Scenario: No scan result when no device is connected
        Given the PicoSlave device is "unplugged"
        When I scan PicoSlave devices
        Then the list of found devices contains 0 devices

        When I search any PicoSlave device
        Then an "DeviceNotFoundError" was thrown that contained "No device found"


    Scenario: Scan 1 PicoSlave when the device is connected
        Given the PicoSlave device is "plugged-in"
        When I scan PicoSlave devices
        Then the list of found devices contains 1 devices


    Scenario: Device found by known serial number
        Given I know the serial number of the PicoSlave device
        When I search a PicoSlave device by serial number "<known>"
        Then the list of found devices contains 1 devices


    Scenario: Device not found with wrong serial number
        When I search a PicoSlave device by serial number "ABCDEF"
        Then an "DeviceNotFoundError" was thrown that contained "No device found with serial number "ABCDEF"


    Scenario: Device found with correct index
        When I search a PicoSlave device at index 0
        Then the list of found devices contains 1 devices


    Scenario: Device not found with wrong index
        When I search a PicoSlave device at index 1
        Then an "DeviceNotFoundError" was thrown that contained "No device with index "1"
