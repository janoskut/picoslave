Feature: Slave Configuration

    Background:
        Given slave "0" is configured for address "0x00"
        Given slave "1" is configured for address "0x00"


    Scenario: Without configuration the bus scan finds no slave
        Then a bus scan finds no slave


    Scenario Outline: Configured slave is seen by bus scan
        When I configure USB slave "<id>" for address "<addr>"
        Then a bus scan finds a slave on address "<addr>"
        Examples:
            | id | addr |
            | 0  | 0x16 |
            | 0  | 0x17 |
            | 1  | 0x18 |
            | 1  | 0x19 |


    Scenario Outline: Reconfiguring a slave
        Given slave "<id>" is configured for address "<init>"
        When I configure USB slave "<id>" for address "<addr>"
        Then a bus scan finds a slave on address "<addr>"
        Examples:
            | id | init | addr |
            | 0  | 0x16 | 0x17 |
            | 0  | 0x17 | 0x16 |
            | 1  | 0x18 | 0x19 |
            | 1  | 0x19 | 0x18 |


    Scenario Outline: Configuring both slaves
        Given slave "0" is configured for address "<addr1>"
        And slave "1" is configured for address "<addr2>"
        Then a bus scan finds a slave on address "<addr1>"
        And a bus scan finds a slave on address "<addr2>"
        Examples:
            | addr1 | addr2 |
            | 0x16  | 0x17  |
            | 0x01  | 0x7F  |
            | 0x01  | 0x02  |
            | 0x19  | 0x18  |


    Scenario: Disabling a slave
        Given slave "0" is configured for address "0x16"
        When I configure USB slave "0" for address "0x00"
        Then a bus scan finds no slave


    Scenario Outline: Illegal interface configuration
        When I configure USB slave "<id>" for address "0x00"
        Then an "<error>" was thrown that contained "<content>"
        Examples:
            | id  | error         | content                  |
            | 2   | ProtocolError | INVALID_INTERFACE        |
            | 3   | ProtocolError | INVALID_INTERFACE        |
            | 100 | ProtocolError | INVALID_INTERFACE        |
            | 300 | ValueError    | must be in range(0, 256) |


    Scenario Outline: Illegal address configuration
        When I configure USB slave "0" for address "<addr>"
        Then an "<error>" was thrown that contained "<content>"
        Examples:
            | addr    | error          | content                          |
            | 0x80    | ProtocolError  | INVALID_ADDRESS                  |
            | 0xFF    | ProtocolError  | INVALID_ADDRESS                  |
            | 0x10000 | AssertionError | addr must be in range [0..2**16] |


    Scenario Outline: Illegal memory size configuration
        When I configure USB slave "0" for address "0x16" with memory size "<size>"
        Then an "<error>" was thrown that contained "<content>"
        Examples:
            | size  | error         | content      |
            | 257   | ProtocolError | INVALID_SIZE |
            | 65535 | ProtocolError | INVALID_SIZE |
