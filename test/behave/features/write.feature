Feature: Write into PicoSlave memory

    Writing into the memory of the PicoSlave via USB has causes:
    1. the data to be read via I2C
    2. the data to be read back via USB

    Background:
        Given slave "0" is configured for address "0x16"
        And slave "0" is cleared


    Scenario Outline: Writing single bytes
        When I write "<value>" to USB slave "0" at address "<addr>"
        Then reading I2C slave "0x16" at address "<addr>" with length "1" gives "<value>"
        And reading I2C slave "0x16" at address "<nextaddr>" with length "1" gives "00"
        And reading USB slave "0" at address "<addr>" with length "1" gives "<value>"
        And reading USB slave "1" at address "<addr>" with length "1" gives "00"
        Examples:
            | addr | nextaddr | value |
            | 0x00 | 0x01     | AB    |
            | 0x00 | 0x01     | BC    |
            | 0x00 | 0x01     | FF    |
            | 0x00 | 0x01     | 00    |
            | 0x01 | 0x02     | 01    |
            | 0xFF | 0x00     | FF    |


    Scenario Outline: Writing multiple single bytes
        When I write "<value1>" to USB slave "0" at address "<addr1>"
        And I write "<value2>" to USB slave "0" at address "<addr2>"
        Then reading I2C slave "0x16" at address "<addr1>" with length "2" gives "<expected>"
        And reading USB slave "0" at address "<addr1>" with length "2" gives "<expected>"
        And reading USB slave "1" at address "<addr1>" with length "2" gives "0000"
        Examples:
            | addr1 | addr2 | value1 | value2 | expected |
            | 0x00  | 0x01  | DE     | AD     | DEAD     |
            | 0xDE  | 0xDF  | BE     | EF     | BEEF     |
            | 0xFE  | 0xFF  | FF     | FF     | FFFF     |


    Scenario Outline: Writing multiple bytes
        When I write "<value>" to USB slave "0" at address "<addr>"
        Then reading I2C slave "0x16" at address "<addr>" with length "<length>" gives "<value>"
        And reading USB slave "0" at address "<addr>" with length "<length>" gives "<value>"
        Examples:
            | addr | value    | length |
            | 0x00 | abcdef   | 3      |
            | 0x00 | ffffffff | 4      |
            | 0xAB | deadbeef | 4      |


    Scenario Outline: Writing many bytes
        When I write "<num>" random bytes to USB slave "0" at address "<addr>"
        Then reading I2C slave "0x16" at address "<addr>" with length "<num>" gives "random_bytes"
        And reading USB slave "0" at address "<addr>" with length "<num>" gives "random_bytes"
        Examples:
            | num | addr |
            | 1   | 0xAB |
            | 10  | 0xAB |
            | 32  | 0x80 |
            | 256 | 0x00 |


    Scenario Outline: Writing out of bounds gives overflow error
        When I write "<data>" to USB slave "0" at address "<addr>"
        Then an "ProtocolError" was thrown that contained "MEMORY_ERROR"
        Examples:
            | data  | addr |
            | abcd  | 0x3FF  |
            | deadbeef  | 0x3FD  |
            | 00000000000000000000000000000000 | 0x3F1 |


    Scenario: Independent slaves from USB
        # Regression test to verify that the memory areas of two slaves are independent
        Given slave "1" is configured for address "0x23"
        And slave "1" is cleared
        When I write "1122334455667788" to USB slave "0" at address "0xab"
        And I write "1234567823456789" to USB slave "1" at address "0xcd"
        Then reading I2C slave "0x16" at address "0xab" with length "8" gives "1122334455667788"
        Then reading I2C slave "0x23" at address "0xab" with length "8" gives "0000000000000000"
        And reading I2C slave "0x23" at address "0xcd" with length "8" gives "1234567823456789"
        And reading I2C slave "0x16" at address "0xcd" with length "8" gives "0000000000000000"
        Then reading USB slave "0" at address "0xab" with length "8" gives "1122334455667788"
        Then reading USB slave "1" at address "0xab" with length "8" gives "0000000000000000"
        And reading USB slave "1" at address "0xcd" with length "8" gives "1234567823456789"
        And reading USB slave "0" at address "0xcd" with length "8" gives "0000000000000000"


    Scenario: Independent slaves from I2C
        # Regression test to verify that the memory areas of two slaves are independent
        Given slave "1" is configured for address "0x23"
        And slave "1" is cleared
        When I write "1122334455667788" to I2C slave "0x16" at address "0xab"
        And I write "1234567823456789" to I2C slave "0x23" at address "0xcd"
        Then reading I2C slave "0x16" at address "0xab" with length "8" gives "1122334455667788"
        Then reading I2C slave "0x23" at address "0xab" with length "8" gives "0000000000000000"
        And reading I2C slave "0x23" at address "0xcd" with length "8" gives "1234567823456789"
        And reading I2C slave "0x16" at address "0xcd" with length "8" gives "0000000000000000"
        Then reading USB slave "0" at address "0xab" with length "8" gives "1122334455667788"
        Then reading USB slave "1" at address "0xab" with length "8" gives "0000000000000000"
        And reading USB slave "1" at address "0xcd" with length "8" gives "1234567823456789"
        And reading USB slave "0" at address "0xcd" with length "8" gives "0000000000000000"
