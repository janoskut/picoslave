Feature: Configurable memory word width

    The word width of the programmable I2C memory is configurable for different word sizes
    (1, 2 and 4 bytes). The addressable memory is limited to 256 Bytes, but behind each address
    stored is the number of bytes as configured, which is referred to as the word width or memory
    width.

    Background: Slave 0 is cleared, but not configured
        Given slave "0" is cleared


    Scenario: I2C read: mem size 4, width 2
        Given slave "0" is configured for address "0x16", size "4" and width "2"
        When I write "aabbccddeeff1122" to USB slave "0" at address "0x00"
        Then reading I2C slave "0x16" at address "0x00" with length "2" gives "aabb"
        And reading I2C slave "0x16" at address "0x01" with length "2" gives "ccdd"
        And reading I2C slave "0x16" at address "0x02" with length "2" gives "eeff"
        And reading I2C slave "0x16" at address "0x03" with length "2" gives "1122"
        And reading I2C slave "0x16" at address "0x00" with length "8" gives "aabbccddeeff1122"
        And reading I2C slave "0x16" at address "0x00" with length "10" gives "aabbccddeeff1122aabb"
        When I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0:(4,0), 1:(3,0), 2:(3,0), 3:(3,0)}"


    Scenario: I2C read: mem size 2, width 4
        Given slave "0" is configured for address "0x16", size "2" and width "4"
        When I write "aabbccddeeff1122" to USB slave "0" at address "0x00"
        Then reading I2C slave "0x16" at address "0x00" with length "4" gives "aabbccdd"
        And reading I2C slave "0x16" at address "0x01" with length "4" gives "eeff1122"
        And reading I2C slave "0x16" at address "0x00" with length "8" gives "aabbccddeeff1122"
        And reading I2C slave "0x16" at address "0x00" with length "10" gives "aabbccddeeff1122aabb"
        When I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0:(4,0), 1:(3,0)}"


    Scenario: I2C write, I2C read: mem size 4, width 2
        Given slave "0" is configured for address "0x16", size "4" and width "2"
        When I write "aabbccddeeff1122" to I2C slave "0" at address "0x00"
        Then reading I2C slave "0x16" at address "0x00" with length "2" gives "aabb"
        And reading I2C slave "0x16" at address "0x01" with length "2" gives "ccdd"
        And reading I2C slave "0x16" at address "0x02" with length "2" gives "eeff"
        And reading I2C slave "0x16" at address "0x03" with length "2" gives "1122"
        And reading I2C slave "0x16" at address "0x00" with length "8" gives "aabbccddeeff1122"
        And reading I2C slave "0x16" at address "0x00" with length "10" gives "aabbccddeeff1122aabb"
        When I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0:(4,1), 1:(3,1), 2:(3,1), 3:(3,1)}"


    Scenario: I2C write, I2C read: mem size 2, width 4
        Given slave "0" is configured for address "0x16", size "2" and width "4"
        When I write "aabbccddeeff1122" to I2C slave "0" at address "0x00"
        Then reading I2C slave "0x16" at address "0x00" with length "4" gives "aabbccdd"
        And reading I2C slave "0x16" at address "0x01" with length "4" gives "eeff1122"
        And reading I2C slave "0x16" at address "0x00" with length "8" gives "aabbccddeeff1122"
        And reading I2C slave "0x16" at address "0x00" with length "10" gives "aabbccddeeff1122aabb"
        When I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0:(4,1), 1:(3,1)}"


    Scenario: I2C write, USB read: mem size 4, width 2
        Given slave "0" is configured for address "0x16", size "4" and width "2"
        When I write "aabbccddeeff1122" to I2C slave "0" at address "0x00"
        Then reading USB slave "0" at address "0x00" with length "2" gives "aabb"
        And reading USB slave "0" at address "0x01" with length "2" gives "bbcc"
        And reading USB slave "0" at address "0x02" with length "2" gives "ccdd"
        And reading USB slave "0" at address "0x03" with length "2" gives "ddee"
        And reading USB slave "0" at address "0x04" with length "2" gives "eeff"
        And reading USB slave "0" at address "0x05" with length "2" gives "ff11"
        And reading USB slave "0" at address "0x06" with length "2" gives "1122"
        And reading USB slave "0" at address "0x07" with length "2" gives "2200"
        And reading USB slave "0" at address "0x00" with length "8" gives "aabbccddeeff1122"
        When I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0:(0,1), 1:(0,1), 2:(0,1), 3:(0,1)}"


    Scenario: I2C write, USB read: mem size 4, width 4
        Given slave "0" is configured for address "0x16", size "4" and width "4"
        When I write "aabbccdd" to I2C slave "0" at address "0x00"
        And I write "eeff1122" to I2C slave "0" at address "0x01"
        Then reading USB slave "0" at address "0x00" with length "8" gives "aabbccddeeff1122"
        When I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0:(0,1), 1:(0,1)}"


    Scenario: I2C write, USB read: mem size 4, width 4
        Given slave "0" is configured for address "0x16", size "4" and width "4"
        When I write "aabbccdd" to I2C slave "0" at address "0x00"
        And I write "eeff1122" to I2C slave "0" at address "0x01"
        Then reading USB slave "0" at address "0x00" with length "8" gives "aabbccddeeff1122"
        When I read "256" statistics entries from USB slave "0" at address "0x00"
        Then statistics contains "{0:(0,1), 1:(0,1)}"


    Scenario Outline: Read/Write random addresses
        Given slave "0" is configured for address "0x16", size "256" and width "<width>"
        When I write "<value>" to USB slave "0" at address "<usb_addr>"
        Then reading I2C slave "0x16" at address "<i2c_addr>" with length "4" gives "<value>"
        Examples:
            | width | value    | usb_addr | i2c_addr |
            | 1     | cafeaffe | 0xFB     | 0xFB     |
            | 1     | affeeffa | 0x7F     | 0x7F     |
            | 1     | 11223344 | 0x00     | 0x00     |
            | 1     | 55667788 | 0x01     | 0x01     |
            | 2     | cafeaffe | 0x1FC    | 0xFE     |
            | 2     | affeeffa | 0xFE     | 0x7F     |
            | 2     | 11223344 | 0x00     | 0x00     |
            | 2     | 55667788 | 0x02     | 0x01     |
            | 4     | cafeaffe | 0x3FC    | 0xFF     |
            | 4     | affeeffa | 0x1FC    | 0x7F     |
            | 4     | 11223344 | 0x00     | 0x00     |
            | 4     | 55667788 | 0x04     | 0x01     |
