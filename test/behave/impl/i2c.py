import logging
from typing import List

from smbus2 import SMBus

from impl.util import bytestr


log = logging.getLogger(__name__)


class I2c:

    def __init__(self, bus_id: int) -> None:
        """
        Args:
            bus_id (int): The I2C Bus ID to operate on for this instance. Available busses can be
                retrieved using `sudo i2cdetect -l`, which lists lines starting with "i2c-1", in
                which case the `bus_id` would be "1".
        """
        self._bus = SMBus(bus_id)

    def bus_scan(self) -> List[int]:
        log.info('performing bus scan...')
        slaves = []
        for addr in range(1, 128):
            try:
                self._bus.read_byte_data(addr, 1)
            except OSError:
                continue
            slaves.append(addr)
        log.info(f'[bus_scan] found slaves: {slaves}')
        return slaves

    def read(self, slave_addr: int, mem_addr: int, length: int) -> bytes:
        log.debug(f'[read] slave_addr: {slave_addr:02x}h, mem_addr: 0x{mem_addr:02X}, '
                  f'length: {length}')
        result = bytearray()
        while length > 0:
            read_len = min(32, length)  # max r/w number for I2C
            read = self._bus.read_i2c_block_data(slave_addr, mem_addr, read_len)
            result.extend(read)
            length -= read_len
            mem_addr += read_len
        log.debug(f'[read] {bytestr(result)}')
        return bytes(result)

    def write(self, slave_addr: int, mem_addr: int, data: bytes) -> None:
        log.debug(f'[write] slave_addr: {slave_addr:02x}h, mem_addr: 0x{mem_addr:02X}, '
                  f'length: {len(data)}')
        log.debug(f'[write] {bytestr(data)}')
        int_data = [int(b) for b in data]
        self._bus.write_i2c_block_data(slave_addr, mem_addr, int_data)


def main() -> None:
    i2c = I2c(1)
    slaves = i2c.bus_scan()
    for addr in slaves:
        print(f'{addr:02x}h: found slave')

    i2c.write(0x16, 0, bytes([2, 3]))


if __name__ == '__main__':
    main()
