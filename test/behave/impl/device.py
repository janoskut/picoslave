import time
from typing import Optional

from .usb import UsbHub

from picoslave.picoslave import DeviceNotFoundError, PicoSlave, UsbError


class PicoSlaveDevice():

    def __init__(self, usb_hub: UsbHub) -> None:
        self._usb_hub = usb_hub
        self._picoslave: Optional[PicoSlave] = None

    @property
    def picoslave(self) -> PicoSlave:
        if self._picoslave is None:
            print('WARNING: device is unplugged - plugging in to obtain picoslave')
            self.plugin()
            assert self._picoslave is not None  # guaranteed by self.plugin()
        return self._picoslave

    def plugin(self) -> None:
        if self._picoslave:
            return
        self._usb_hub.plugin_device()
        time.sleep(0.1)
        self._picoslave = self._find_picoslave(attempts=10)

    def unplug(self) -> None:
        if not self._picoslave:
            return
        self._picoslave.close()
        self._picoslave = None
        self._usb_hub.unplug_device()

    def reset(self) -> None:
        if self._picoslave:
            self._picoslave.close()
        self._usb_hub.reset_device(cycle_delay=0.1)
        time.sleep(0.1)
        self._picoslave = self._find_picoslave(attempts=10)

    def close(self) -> None:
        if self._picoslave:
            self._picoslave.close()

    def _find_picoslave(self, attempts: int) -> PicoSlave:
        last_exc = None
        for _ in range(attempts):
            try:
                return PicoSlave()
            except (UsbError, DeviceNotFoundError) as exc:
                last_exc = exc
                print('faild to find PicoSlave USB device... retrying')
            time.sleep(0.2)
        raise OSError('failed to open PicoSlave USB device') from last_exc
