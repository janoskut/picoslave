import logging
from typing import Callable, List


log = logging.getLogger(__name__)


def bytestr(b: bytes) -> str:
    return ''.join(f'{x:02X}' for x in b)


def retry(func: Callable[[], None], times: int) -> List[Exception]:
    exceptions = []
    for _ in range(times):
        try:
            func()
            return []
        except Exception as exc:
            exceptions.append(exc)
            log.error(exc)
    return exceptions
