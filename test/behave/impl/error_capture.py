import textwrap
import traceback
from typing import Any, Callable, List

from behave.runner import Context

from hamcrest import assert_that


class ErrorCapture:

    def __init__(self, context: Context, exc: Exception) -> None:
        self._exc = exc
        self._feature = context.feature if 'feature' in context else None
        self._scenario = context.scenario if 'scenario' in context else None
        self._step = context.step if 'step' in context else None
        self._traceback = traceback.format_exc()

    def __str__(self) -> str:
        return f'Exception:     {type(self._exc)}\n' \
               f'+   Message:   {str(self._exc)}\n' \
               f'+   Feature:   {self._feature or "unknown"}\n' \
               f'+   Scenario:  {self._scenario or "unknown"}\n' \
               f'+   Step:      {self._step or "unknown"}\n' \
               f'{textwrap.indent(self._traceback, "+   ")}'


def capture(func: Callable[..., None]) -> Callable[..., None]:
    def wrapper(context: Context, *args: Any, **kwargs: Any) -> None:
        try:
            func(context, *args, **kwargs)
        except Exception as e:
            context.captured_errors.append(ErrorCapture(context, e))
    return wrapper


def expect(context: Context, matcher: Callable[[Exception], bool]) -> bool:
    orig_len = len(context.captured_errors)
    context.captured_errors[:] = \
        [capture for capture in context.captured_errors if not matcher(capture._exc)]
    return len(context.captured_errors) < orig_len


def init(context: Context) -> None:
    context.captured_errors = []  # List[ErrorCapture]


def teardown(context: Context) -> None:
    assert_that(not context.captured_errors, f'There were remaining captured errors: \n'
                                             f'{to_str(context.captured_errors)}')


def to_str(captures: List[ErrorCapture]) -> str:
    return textwrap.indent('\n'.join([str(c) for c in captures]), '    ')
