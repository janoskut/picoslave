from pathlib import Path
import shlex
import shutil
import subprocess
from typing import List


class UsbHub:

    def __init__(self, hub_location: str, hub_ports: List[int] = []) -> None:
        """

        Args:
            hub_location (str): USB HUB location of the PicoSlave device. Run `sudo uhubctl` to find
                a line such as "Current status for hub 1-1 ...", under which the PicoSlave device
                is listed. Then "1-1" is the hub-location.
            hub_ports (list[int]): device port locations on hub.
        """
        self._hub_location = hub_location
        self._hub_ports_str = \
            f'--ports {",".join([str(p) for p in hub_ports])}' if hub_ports else ''

    @staticmethod
    def _cmd(cmd: str) -> 'subprocess.CompletedProcess[bytes]':
        print(f'executing command: {cmd}')
        return subprocess.run(shlex.split(cmd), shell=False,
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    def _reset_openocd(self) -> None:
        picoflash_sh = Path(__file__).parent.parent.parent.parent / "util" / "picoflash.sh"
        result = self._cmd(f'{str(picoflash_sh)} --reset')
        if result.returncode != 0:
            raise ChildProcessError(f'OpenOCD reset failed: {result.stderr.decode()}')

    def _uhubctl(self, args: str) -> None:
        if not shutil.which('uhubctl'):
            raise ProcessLookupError('`uhubctl` not found on system. '
                                     'Cannot perform PicoSlave reset.')

        result = self._cmd(f'uhubctl --level {self._hub_location} {self._hub_ports_str} ' + args)
        if result.returncode != 0:
            raise ChildProcessError(f'uhubctl reset failed: {result.stderr.decode()}')

    def reset_device(self, cycle_delay: float = 0.1) -> None:
        self._uhubctl(args=f'--action cycle --delay {cycle_delay}')

    def plugin_device(self) -> None:
        self._uhubctl(args='--action on')

    def unplug_device(self) -> None:
        self._uhubctl(args='--action off')


def main() -> None:
    import argparse

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('location', type=str, help='USB hub location')
    parser.add_argument(
        'action',
        nargs='?',
        type=str,
        choices=['reset', 'plugin', 'unplug'],
        help='reset, power off or power on the USB hub',
    )
    args = parser.parse_args()

    usb = UsbHub(hub_location=args.location)

    if args.action == 'reset':
        usb.reset_device()
    elif args.action == 'plugin':
        usb.plugin_device()
    elif args.action == 'unplug':
        usb.unplug_device()


if __name__ == '__main__':
    main()
