

# Raspberry Pi Configuration

To configure the Raspberry Pi as a GitLab test runner, the following configurations need to be done.

## System Dependencies

These packages aren't required, but useful to have
```sh
sudo apt-get update
sudo apt-get -y install zsh git vim tmux picocom minicom tio tree
```

## OpenOCD

`OpenOCD` is needed for flashing test binaries to the PicoSlave target.

--> See main `README.md` for how to install.

## Control USB power

Our `behave` tests start by resetting the test target, i.e. the PicoSlave USB device. We do this
by controlling the Raspberry Pi's root hubs output power, using [uhubctl](https://github.com/mvp/uhubctl).

### `uhubctl` setup
```sh
sudo apt-get install libusb-1.0-0-dev
git clone https://github.com/mvp/uhubctl
cd uhubctl
make -j$(nproc)
sudo mv uhubctl /usr/local/bin
```
Add Raspberry Pi Root hub vendor ID to udev:
```sh
sudo cp ./contrib/99-rpi-roothub.rules /etc/udev/rules.d/
sudo udevadm control --reload
sudo udevadm trigger --attr-match=subsystem=usb
```

### `uhubctl` example usage
Operate the power supply:
```sh
# Find your device under hub <N> such as "Current status for hub <N> ..."
uhubctl
# on USB 3 port
uhubctl -l 2 -a off/on/cycle/toggle
# on USB 2 port
uhubctl -l 1-1 -a off/on/cycle/toggle
```
_(note: this doesn't work anymore on Raspberry Pi OS bullseye - both pico devices are listed
under the USB 2.0 hub "1-1", so they can't be power cycled individually.)_

## Configure I2C

I2C needs to be enabled in `/boot/config.txt`, and I2C busrate should be set to max in order to
prevent glitches on the line.
```
# file: /boot/config.txt

dtparam=i2c_arm=on,i2c_arm_baudrate=400000
```
(reboot after changing).

## Gitlab runner setup
```sh
# install runner
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_armhf.deb"
sudo dpkg -i gitlab-runner_armhf.deb

RUNNER_NAME="picoslave-rpi"
REGISTRATION_TOKEN=<registration-token>

# register runner
sudo gitlab-runner register --non-interactive \
    --name ${RUNNER_NAME} \
    --url https://gitlab.com/ \
    --registration-token ${REGISTRATION_TOKEN} \
    --tag-list picoslave-runner \
    --locked=false \
    --executor shell
```

### User `gitlab-runner` setup
```sh
# add to groups
sudo usermod -a -G plugdev,i2c gitlab-runner

# install virtualenv
sudo su gitlab-runner
python -m pip install --upgrade pip
python -m pip install virtualenv
exit
```

## Manual I2C operation (cheat sheet)
```sh
sudo apt install i2c-tools
```
```sh
i2cdetect -l                    # list busses
i2cdetect -y 1                  # scan for slaves on bus 1
i2cget -y 1 0x16 0x00           # read a byte from slave 0x16 at register 0x00
i2cget -y 1 0x16 0x00 w         # read a word from slave 0x16 at register 0x00
i2cset -y 1 0x16 0x00 0xab      # write 0xab to slave 0x16 at register 0x00
i2cset -y 1 0x16 0x00 0xabcd w  # write 0xabcd to slave 0x16 at register 0x00
```
