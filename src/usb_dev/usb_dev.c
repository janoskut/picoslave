#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <pico/assert.h>

#include <tusb.h>

#include "crc.h"
#include "i2c.h"
#include "led.h"
#include "log.h"
#include "picoslave_config.h"
#include "protocol.h"
#include "usb_dev.h"


/**************************************************************************************************/
/* Definitions */
/**************************************************************************************************/

#define CRC_SIZE (sizeof(uint16_t))
#define HEXDUMP_BUF_SIZE (128u)

#define USB_RX_BUF_SIZE (CFG_TUD_VENDOR_RX_BUFSIZE)
#define USB_TX_BUF_SIZE (CFG_TUD_VENDOR_TX_BUFSIZE)


/**************************************************************************************************/
/* Types */
/**************************************************************************************************/

/**************************************************************************************************/
/* Prototypes */
/**************************************************************************************************/

static size_t usb_handle_packet(uint8_t const* buf, size_t len, uint8_t* out, size_t out_max_len);
static size_t create_error_response(usb_rsp_code_t code,  uint8_t* out, size_t out_max_len);

/**************************************************************************************************/
/* Globals */
/**************************************************************************************************/

static usb_ctrl_t const* USB_CTRL = NULL;
static char HEXDUMP_BUF[HEXDUMP_BUF_SIZE];

static struct {
    size_t  rx_len;
    uint8_t rx_buf[USB_RX_BUF_SIZE];
    size_t  tx_len;
    uint8_t const* tx_pos;
    uint8_t tx_buf[USB_TX_BUF_SIZE];
} usb;


/**************************************************************************************************/
/* Private Implementation */
/**************************************************************************************************/

static char const* usb_cmd_to_string(usb_cmd_t cmd) {
    switch (cmd) {
        case USB_CMD_CONFIG:
            return "CONFIG";
        case USB_CMD_READ:
            return "READ";
        case USB_CMD_WRITE:
            return "WRITE";
        case USB_CMD_CLEAR:
            return "CLEAR";
        case USB_CMD_STAT:
            return "STAT";
        case USB_CMD_INFO:
            return "INFO";
        case USB_CMD_RESET:
            return "RESET";
        }
    return "UNKNOWN";
}
static void hexdump(const uint8_t* ibuf, unsigned ibuf_len, char* obuf, unsigned obuf_size)
{
    obuf[0] = '\0';
    if (LOG_LEVEL == LOG_TRACE) {
        for (unsigned i = 0; i < ibuf_len && i < obuf_size / 3; i++) {
            const char* fmt = i < ibuf_len - 1 ? "%02X " : "%02X";
            obuf += snprintf(obuf, obuf_size, fmt, ibuf[i]);
        }
    }
}

static size_t usb_handle_wire_packet(uint8_t const* buf, size_t len, uint8_t* out, size_t out_max_len) {

    size_t out_size = 0;

    // check CRC before touching any part of the packet
    uint8_t const* crc_addr = buf + len - CRC_SIZE;
    uint16_t packet_crc = ((*(crc_addr + 1)) << 8) + *crc_addr;
    uint16_t crc = crc16(buf, len - CRC_SIZE);
    log_trace("[usb rx] CRC: packet: 0x%02X, calc: 0x%02X", packet_crc, crc);

    if (packet_crc != crc) {
        log_warn("[usb rx] CRC mismatch: packet: %04xh, calc: %04xh", packet_crc, crc);
        out_size = create_error_response(USB_RSP_CODE_CRC_ERROR, out, out_max_len - CRC_SIZE);
    }
    else {
        log_debug("[usb rx] CRC checksum match");

        out_size = usb_handle_packet(buf, len - CRC_SIZE, out, out_max_len - CRC_SIZE);
    }

    // Calculate CRC and write into the buffer as well
    crc = crc16(out, out_size);
    memcpy(out + out_size, (uint8_t const*) &crc, CRC_SIZE);

    size_t response_size = out_size + CRC_SIZE;
    hexdump(out, response_size, HEXDUMP_BUF, HEXDUMP_BUF_SIZE);
    log_trace("tx: %s", HEXDUMP_BUF);

    return response_size;
}

static size_t create_error_response(usb_rsp_code_t error_code, uint8_t* out, size_t out_max_len) {
    assert(out_max_len >= sizeof(usb_rsp_pkt_hdr_t));

    usb_rsp_pkt_hdr_t rsp_header = {
        .code = error_code,
        .size = 0,
    };
    memcpy(out, &rsp_header, sizeof(usb_rsp_pkt_hdr_t));
    return sizeof(usb_rsp_pkt_hdr_t);
}

static usb_rsp_code_t usb_analyze_host_packet(
    uint8_t const* buf, size_t len, usb_host_pkt_t* packet) {

    packet->header = (usb_host_pkt_hdr_t*) buf;
    packet->data = NULL;

    size_t data_size = len - sizeof(usb_host_pkt_hdr_t);

    if (packet->header->iface >= PICOSLAVE_I2C_NUM_SLAVES) {
        return USB_RSP_CODE_INVALID_INTERFACE;
    }

    if (packet->header->cmd == USB_CMD_READ
        || packet->header->cmd == USB_CMD_WRITE
        || packet->header->cmd == USB_CMD_STAT) {

        if (packet->header->size == 0) {
            log_warn("received zero size in READ/WRITE/STAT packet (size 0)");
            return USB_RSP_CODE_INVALID_SIZE;
        }
    }

    if (packet->header->cmd == USB_CMD_READ
        || packet->header->cmd == USB_CMD_STAT
        || packet->header->cmd == USB_CMD_CLEAR) {

        if (data_size > 0) {
            log_warn("received unexpected data for READ/STAT packet (data_size %u)", data_size);
            return USB_RSP_CODE_INVALID_SIZE;
        }
    }

    if (packet->header->cmd == USB_CMD_CONFIG
        || packet->header->cmd == USB_CMD_WRITE) {

        if (packet->header->size != data_size) {
            log_warn("size doesn't match packet len (%u != %u)", data_size, packet->header->size);
            return USB_RSP_CODE_INVALID_SIZE;
        }

        if (packet->header->size > 0) {
            packet->data = buf + sizeof(usb_host_pkt_hdr_t);
        }
    }

    return USB_RSP_CODE_OK;
}

static size_t usb_handle_packet(uint8_t const* buf, size_t len, uint8_t* out, size_t out_max_len) {
    assert(buf != NULL);
    assert(out != NULL);

    usb_host_pkt_t rx_packet;
    usb_rsp_code_t code = usb_analyze_host_packet(buf, len, &rx_packet);

    if (code != USB_RSP_CODE_OK) {
        return create_error_response(code, out, out_max_len);
    }

    usb_host_pkt_hdr_t const* header = rx_packet.header;

    usb_cmd_t  cmd   = header->cmd;
    uint8_t    iface = header->iface;
    uint16_t   addr  = header->addr;
    uint16_t   size  = header->size;

    // for reading commands
    uint8_t* read_buf      = out + sizeof(usb_rsp_pkt_hdr_t);
    size_t   max_read_size = out_max_len - sizeof(usb_rsp_pkt_hdr_t);

    size_t data_size = 0;

    switch (cmd) {
        case USB_CMD_CONFIG: {
            log_info("[CONFIG] - iface=%u, slave_address=%Xh", iface, addr);
            code = USB_CTRL->config(iface, addr, rx_packet.data, size);
        }
        break;
        case USB_CMD_READ: {
            log_info("[READ] - iface=%u, addr=%Xh, size=%u", iface, addr, size);
            if ((size * sizeof(uint8_t)) > max_read_size) {
                log_warn("requested READ size larger than allowed: %u > %u", size, max_read_size);
                code = USB_RSP_CODE_INVALID_SIZE;
            } else {
                code = USB_CTRL->read(iface, addr, read_buf, size);
                if (code == USB_RSP_CODE_OK) {
                    data_size = size;
                }
            }
        }
        break;
        case USB_CMD_WRITE: {
            hexdump(rx_packet.data, size, HEXDUMP_BUF, HEXDUMP_BUF_SIZE);
            log_info("[WRITE] - iface=%u, addr=%Xh, size=%u, data='%s'",
                iface, addr, size, HEXDUMP_BUF);
            code = USB_CTRL->write(iface, addr, rx_packet.data, size);
        }
        break;
        case USB_CMD_CLEAR: {
            log_info("[CLEAR] - iface=%u, reset_value=%Xh", iface, addr);
            code = USB_CTRL->clear(iface, addr);
        }
        break;
        case USB_CMD_STAT: {
            log_info("[STAT] - iface=%u, addr=%Xh, size=%u", iface, addr, size);
            if ((size * sizeof(usb_stat_t)) > max_read_size) {
                log_warn("requested STAT size larger than allowed: %u*%u > %u",
                    size, sizeof(usb_stat_t), max_read_size);
                code = USB_RSP_CODE_INVALID_SIZE;
            } else {
                code = USB_CTRL->stat(iface, addr, read_buf, size);
                if (code == USB_RSP_CODE_OK) {
                    data_size = size * sizeof(usb_stat_t);
                }
            }
        }
        break;
        case USB_CMD_INFO: {
            log_info("[INFO] - command received. Gathering info");
            size_t out_size = 0;
            code = USB_CTRL->info(read_buf, max_read_size, &out_size);
            if (code == USB_RSP_CODE_OK) {
                data_size = out_size;
            }
        }
        break;
        case USB_CMD_RESET: {
            log_info("[RESET] - command received. Initiating reset");
            code = USB_CTRL->reset();
        }
        break;
        default: {
            log_warn("Unknown command: 0x%02X.", cmd);
            code = USB_RSP_CODE_INVALID_REQUEST;
        }
        break;
    }

    if (code != USB_RSP_CODE_OK) {
        log_warn("[%s] failed with code: %u", usb_cmd_to_string(cmd), code);
    } else {
        log_info("[%s] Success.", usb_cmd_to_string(cmd));
    }

    usb_rsp_pkt_hdr_t rsp_header = {
        .code = code,
        .size = data_size,
    };

    // write the packet header into the 'out' buffer. The 'data' should be already there
    memcpy(out, &rsp_header, sizeof(usb_rsp_pkt_hdr_t));

    return sizeof(usb_rsp_pkt_hdr_t) + data_size;
}

void usb_write_remaining(void) {
    if (tud_vendor_write_available()) {
        size_t pkt_size = usb.tx_len > CFG_TUD_VENDOR_TX_BUFSIZE
                        ? CFG_TUD_VENDOR_TX_BUFSIZE : usb.tx_len;
        tud_vendor_write(usb.tx_pos, pkt_size);
        hexdump(usb.tx_pos, pkt_size, HEXDUMP_BUF, HEXDUMP_BUF_SIZE);
        log_trace("[usb tx]: %s", HEXDUMP_BUF);
        usb.tx_pos += pkt_size;
        usb.tx_len -= pkt_size;
    }
}


/**************************************************************************************************/
/* Public Implementation */
/**************************************************************************************************/

void usb_init(usb_ctrl_t const* ctrl) {
    assert(ctrl->config != NULL);
    assert(ctrl->read != NULL);
    assert(ctrl->write != NULL);
    assert(ctrl->reset != NULL);

    USB_CTRL = ctrl;

    usb.rx_len = 0;
    usb.tx_len = 0;
    usb.tx_pos = (uint8_t const*) usb.tx_buf;
}

void usb_task(void) {

    if (usb.tx_len > 0) {
        usb_write_remaining();
        led_blink();
    }
    else if (tud_vendor_available()) {
        uint32_t count = tud_vendor_read(&usb.rx_buf[usb.rx_len], USB_RX_BUF_SIZE);
        log_trace("[usb rx] %u bytes", count);
        if (count == 0) {
            return;
        }
        led_blink();
        usb.rx_len += count;

        if (usb.rx_len >= sizeof(usb_wire_header_t)) {
            usb_wire_header_t* header = (usb_wire_header_t*) usb.rx_buf;

            log_debug("[usb rx] received packet: %u bytes", usb.rx_len);
            log_trace("rx_len: %u, header->length: %u", usb.rx_len, header->length);

            if (usb.rx_len >= header->length + sizeof(usb_wire_header_t)) {

                hexdump(usb.rx_buf, usb.rx_len, HEXDUMP_BUF, HEXDUMP_BUF_SIZE);
                log_trace("[usb rx] %s", HEXDUMP_BUF);

                size_t out_size = usb_handle_wire_packet(
                    (uint8_t const*) usb.rx_buf + sizeof(usb_wire_header_t),
                    usb.rx_len - sizeof(usb_wire_header_t),
                    (uint8_t*) usb.tx_buf + sizeof(usb_wire_header_t),
                    USB_TX_BUF_SIZE - sizeof(usb_wire_header_t)
                );
                usb.rx_len = 0;

                header = (usb_wire_header_t*) usb.tx_buf;
                header->length = out_size;
                usb.tx_len = out_size + sizeof(usb_wire_header_t);
                usb.tx_pos = (uint8_t const*) usb.tx_buf;
                log_debug("[usb tx]: %u bytes", usb.tx_len);
                usb_write_remaining();
            }
        }
    }
}
