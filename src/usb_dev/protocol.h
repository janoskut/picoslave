#ifndef USB_H_
#define USB_H_

#include <stdint.h>

/**************************************************************************************************/
/* Definitions */
/**************************************************************************************************/

#define USB_VERSION "1.1"

/**************************************************************************************************/
/* Types */
/**************************************************************************************************/

typedef enum __attribute__ ((__packed__)) {
    USB_CMD_CONFIG = 0xA0,
    USB_CMD_READ   = 0xA1,
    USB_CMD_WRITE  = 0xA2,
    USB_CMD_CLEAR  = 0xA3,
    USB_CMD_STAT   = 0xA4,
    USB_CMD_INFO   = 0xB0,
    USB_CMD_RESET  = 0xBF,
} usb_cmd_t;

typedef enum __attribute__ ((__packed__)) {
    USB_RSP_CODE_OK                = 0,
    USB_RSP_CODE_CRC_ERROR         = 1,
    USB_RSP_CODE_INVALID_PACKET    = 2,
    USB_RSP_CODE_INVALID_REQUEST   = 3,
    USB_RSP_CODE_INVALID_INTERFACE = 4,
    USB_RSP_CODE_INVALID_ADDRESS   = 5,
    USB_RSP_CODE_INVALID_SIZE      = 6,
    USB_RSP_CODE_INVALID_WIDTH     = 7,
    USB_RSP_CODE_MEMORY_ERROR      = 8,
    USB_RSP_CODE_OPERATION_FAILED  = 9,
} usb_rsp_code_t;

typedef struct __attribute__ ((packed)) {
    uint32_t length;
} usb_wire_header_t;

typedef struct __attribute__ ((packed)) {
    usb_cmd_t cmd;
    uint8_t iface;
    uint16_t addr;
    uint16_t size;
} usb_host_pkt_hdr_t;

/* Container to hold command specific values from received packets.
 *
 * Values are only valid specific to the command, e.g. the `WRITE` command will have a valid `data`
 * field and the `CONFIG` command will have a valid `width` field after parsing the received
 * packet. */
typedef struct {
    usb_host_pkt_hdr_t const* header;
    uint8_t const* data;
} usb_host_pkt_t;

typedef struct __attribute__ ((packed)) {
    usb_rsp_code_t code;
    uint16_t size;
} usb_rsp_pkt_hdr_t;

typedef struct __attribute__ ((packed)) {
    uint32_t read_cnt;
    uint32_t write_cnt;
} usb_stat_t;

typedef struct __attribute__ ((packed)) {
    uint16_t mem_size;
    uint16_t mem_width;
} usb_config_data_slave_function_t;

typedef struct __attribute__ ((packed)) {
    bool block_scl;
    bool block_sda;
} usb_config_data_blocker_function_t;


/**************************************************************************************************/
/* Public Interface */
/**************************************************************************************************/

#endif /* USB_H_ */
