#ifndef USB_DEV_H_
#define USB_DEV_H_

#include <stdint.h>
#include <stdlib.h>

#include "protocol.h"


/**************************************************************************************************/
/* Definitions */
/**************************************************************************************************/

/**************************************************************************************************/
/* Types */
/**************************************************************************************************/

typedef usb_rsp_code_t (*usb_config_cb_t)(uint8_t iface, uint8_t slave_address,
                                          uint8_t const* data, size_t len);
typedef usb_rsp_code_t (*usb_read_cb_t)  (uint8_t iface, uint16_t addr, uint8_t* out, size_t len);
typedef usb_rsp_code_t (*usb_write_cb_t) (uint8_t iface, uint16_t addr,
                                          uint8_t const* data, size_t len);
typedef usb_rsp_code_t (*usb_clear_cb_t) (uint8_t iface, uint8_t reset_value);
typedef usb_rsp_code_t (*usb_info_cb_t)  (uint8_t* out, size_t max_len, size_t* out_size);
typedef usb_rsp_code_t (*usb_reset_cb_t) (void);

typedef struct usb_ctrl {
    usb_config_cb_t config;
    usb_read_cb_t   read;
    usb_write_cb_t  write;
    usb_clear_cb_t  clear;
    usb_read_cb_t   stat;
    usb_info_cb_t   info;
    usb_reset_cb_t  reset;
} usb_ctrl_t;


/**************************************************************************************************/
/* Public Interface */
/**************************************************************************************************/

void usb_init(usb_ctrl_t const* ctrl);
void usb_task(void);


#endif /* USB_DEV_H_ */
