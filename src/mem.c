#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <pico/critical_section.h>

#include "mem.h"
#include "picoslave_config.h"
#include "protocol.h"


/**************************************************************************************************/
/* Definitions */
/**************************************************************************************************/

#define CRITICAL_SECTION(expressions) { \
    critical_section_enter_blocking(&config->internal->mutex); \
    expressions; \
    critical_section_exit(&config->internal->mutex); \
}


/**************************************************************************************************/
/* Types */
/**************************************************************************************************/

struct __packed_aligned memory_s {
    uint8_t*           mem;
    usb_stat_t*        stat;
    critical_section_t mutex;
};


/**************************************************************************************************/
/* Prototypes */
/**************************************************************************************************/

/**************************************************************************************************/
/* Globals */
/**************************************************************************************************/

static uint8_t    REGISTERS[PICOSLAVE_I2C_NUM_SLAVES][MEMORY_AREA_MAX];
static usb_stat_t STATISTIC[PICOSLAVE_I2C_NUM_SLAVES][MEMORY_SIZE_MAX];

static mem_t    configs[PICOSLAVE_I2C_NUM_SLAVES];
static memory_t internals[PICOSLAVE_I2C_NUM_SLAVES];


/**************************************************************************************************/
/* Private Implementation */
/**************************************************************************************************/

/**************************************************************************************************/
/* Public Implementation */
/**************************************************************************************************/

mem_t* mem_get(uint8_t iface) {
    if (iface >= PICOSLAVE_I2C_NUM_SLAVES) {
        return NULL;
    }
    return &configs[iface];
}

void mem_init(void) {
    memset(&REGISTERS[0], 0, sizeof(REGISTERS));
    memset(&STATISTIC[0], 0, sizeof(STATISTIC));
    for (size_t iface = 0; iface < PICOSLAVE_I2C_NUM_SLAVES; iface++) {
        mem_t* config = mem_get(iface);
        memset(config, 0, sizeof(mem_t));
        config->internal = &internals[iface];
        memset(config->internal, 0, sizeof(memory_t));
        config->internal->mem  = (uint8_t*)    &REGISTERS[iface][0];
        config->internal->stat = (usb_stat_t*) &STATISTIC[iface][0];
        critical_section_init(&config->internal->mutex);
    }
}

mem_t* mem_config(uint8_t iface, size_t size, uint8_t width) {
    mem_t* config = mem_get(iface);
    assert(config != NULL);
    CRITICAL_SECTION(
        config->size = size;
        config->width = width;
        memset(config->internal->stat, 0x00, MEMORY_SIZE_MAX * sizeof(usb_stat_t));
    );
    return config;
}

void mem_clear(mem_t* config, uint8_t reset_value) {
    assert(config != NULL);
    CRITICAL_SECTION(
        memset(config->internal->mem, reset_value, MEMORY_AREA_MAX);
        memset(config->internal->stat, 0x00, MEMORY_SIZE_MAX * sizeof(usb_stat_t));
    );
}

void mem_read(mem_t* config, uint16_t addr, uint8_t* out, size_t len, bool stat) {
    assert(config != NULL);
    CRITICAL_SECTION(
        memcpy(out, config->internal->mem + addr, len);
        if (stat && addr % config->width == 0) {
            config->internal->stat[addr / config->width].read_cnt++;
        }
    );
}

void mem_write(mem_t* config, uint16_t addr, uint8_t const* data, size_t len, bool stat) {
    assert(config != NULL);
    CRITICAL_SECTION(
        memcpy(config->internal->mem + addr, data, len);
        if (stat && addr % config->width == 0) {
            config->internal->stat[addr / config->width].write_cnt++;
        }
    );
}

void mem_stat(mem_t* config, uint16_t addr, uint8_t* out, size_t len) {
    assert(config != NULL);
    CRITICAL_SECTION(
        memcpy(out, config->internal->stat + addr, len * sizeof(usb_stat_t));
    );
}
