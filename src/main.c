#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <pico/stdlib.h>
#include <pico/unique_id.h>

#include <hardware/watchdog.h>
#include <hardware/vreg.h>
#include <hardware/regs/sysinfo.h>

#include <RP2040.h>
#include <tusb.h>
#include <bsp/board.h>

#include "i2c.h"
#include "led.h"
#include "log.h"
#include "log_stdout.h"
#include "mem.h"
#include "picoslave_config.h"
#include "protocol.h"
#include "usb_dev.h"


/**************************************************************************************************/
/* Definitions */
/**************************************************************************************************/

#define MAIN_LOOP_TIME (1000u)  // ms
#define BLINK_TIME (50u)  // ms
#define WATCHDOG_TIMEOUT ((MAIN_LOOP_TIME / 2) * 5)  // ms, main loop time * 2.5
#ifndef WATCHDOGOFF
#define WATCHDOGOFF 0
#endif /* WATCHDOGOFF */

// FIXME: baudrate configurable?
static const uint32_t I2C_BAUDRATE = 400000; // 400 kHz


/**************************************************************************************************/
/* Types */
/**************************************************************************************************/

/**************************************************************************************************/
/* Prototypes */
/**************************************************************************************************/

static usb_rsp_code_t usb_config_callback(uint8_t iface, uint8_t slave_address,
                                          uint8_t const* data, size_t len);
static usb_rsp_code_t usb_read_callback  (uint8_t iface, uint16_t addr, uint8_t* out, size_t len);
static usb_rsp_code_t usb_write_callback (uint8_t iface, uint16_t addr,
                                          uint8_t const* data, size_t len);
static usb_rsp_code_t usb_clear_callback (uint8_t iface, uint8_t reset_value);
static usb_rsp_code_t usb_stat_callback  (uint8_t iface, uint16_t addr, uint8_t* out, size_t len);
static usb_rsp_code_t usb_info_callback  (uint8_t* out, size_t max_len, size_t* out_size);
static usb_rsp_code_t usb_reset_callback (void);

static int64_t timer_reset_handler(alarm_id_t id, void* user_data);


/**************************************************************************************************/
/* Globals */
/**************************************************************************************************/

/* as declared in `picoslave_config.h` */
char g_picoslave_serial[PICO_UNIQUE_BOARD_ID_SIZE_BYTES * 2 + 1];

static const usb_ctrl_t USB_CTRL = {
    .config = usb_config_callback,
    .read   = usb_read_callback,
    .write  = usb_write_callback,
    .clear  = usb_clear_callback,
    .stat   = usb_stat_callback,
    .info   = usb_info_callback,
    .reset  = usb_reset_callback,
};


/**************************************************************************************************/
/* USB Callbacks */
/**************************************************************************************************/

// cppcheck-suppress unusedFunction
static inline bool is_isr(void) {
    return (SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) != 0;
}

static usb_rsp_code_t usb_config_callback(uint8_t iface, uint8_t slave_address,
                                          uint8_t const* data, size_t len) {
    assert(iface < PICOSLAVE_I2C_NUM_SLAVES);

    if ((slave_address > I2C_SLAVE_FUNC_SLAVE) && (slave_address != I2C_SLAVE_FUNC_BLOCKER)) {
        return USB_RSP_CODE_INVALID_ADDRESS;
    }

    di2c_deinit(iface);

    if (slave_address == I2C_SLAVE_FUNC_RESET) {
        log_info("slave I2C-%u disabled", iface);
    }
    else if (slave_address == I2C_SLAVE_FUNC_BLOCKER) {
        if (len != sizeof(usb_config_data_blocker_function_t)) {
            return USB_RSP_CODE_INVALID_SIZE;
        }
        usb_config_data_blocker_function_t const* config =
            (usb_config_data_blocker_function_t const*) data;
        log_info("slave I2C-%u configured as blocker: scl=%u, sda=%u",
            iface, config->block_scl, config->block_sda);
        di2c_init_blocker(iface, config->block_scl, config->block_sda);
    }
    else {
        if (len != sizeof(usb_config_data_slave_function_t)) {
            return USB_RSP_CODE_INVALID_SIZE;
        }

        usb_config_data_slave_function_t const* config =
            (usb_config_data_slave_function_t const*) data;

        if (config->mem_size == 0 || config->mem_size > MEMORY_SIZE_MAX) {
            return USB_RSP_CODE_INVALID_SIZE;
        }
        if (!(config->mem_width == 1 || config->mem_width == 2 || config->mem_width == 4)) {
            return USB_RSP_CODE_INVALID_WIDTH;
        }

        mem_t* mem = mem_config(iface, config->mem_size, config->mem_width);

        di2c_init_slave(
            iface,
            slave_address,
            I2C_BAUDRATE,
            mem);

        log_info("slave I2C-%u configured for address %02xh, memory size %u and memory width %u",
            iface, slave_address, config->mem_size, config->mem_width);
    }

    return USB_RSP_CODE_OK;
}

static usb_rsp_code_t usb_read_callback(uint8_t iface, uint16_t addr, uint8_t* out, size_t len) {
    assert(iface < PICOSLAVE_I2C_NUM_SLAVES);
    assert(out != NULL);
    assert(len > 0);

    if ((addr >= MEMORY_AREA_MAX) || (addr + len > MEMORY_AREA_MAX)) {
        return USB_RSP_CODE_MEMORY_ERROR;
    }

    log_info("reading from I2C-%u memory at address 0x%02X (%u bytes)", iface, addr, len);
    mem_read(mem_get(iface), addr, out, len, false);

    return USB_RSP_CODE_OK;
}

static usb_rsp_code_t usb_write_callback(uint8_t iface, uint16_t addr,
                                         uint8_t const* data, size_t len) {
    assert(iface < PICOSLAVE_I2C_NUM_SLAVES);
    assert(data != NULL);
    assert(len > 0);

    if ((addr >= MEMORY_AREA_MAX) || (addr + len > MEMORY_AREA_MAX)) {
        return USB_RSP_CODE_MEMORY_ERROR;
    }

    log_info("writing to I2C-%u memory at address 0x%02X (%u bytes)", iface, addr, len);
    mem_write(mem_get(iface), addr, data, len, false);

    return USB_RSP_CODE_OK;
}

static usb_rsp_code_t usb_clear_callback(uint8_t iface, uint8_t reset_value) {
    assert(iface < PICOSLAVE_I2C_NUM_SLAVES);

    log_info("clearing I2C-%u memory with value 0x%02X and statistics", iface, reset_value);
    mem_clear(mem_get(iface), reset_value);

    return USB_RSP_CODE_OK;
}

static usb_rsp_code_t usb_stat_callback(uint8_t iface, uint16_t addr, uint8_t* out, size_t len) {
    assert(iface < PICOSLAVE_I2C_NUM_SLAVES);
    assert(out != NULL);
    assert(len > 0);

    if ((addr >= MEMORY_SIZE_MAX) || (addr + len > MEMORY_SIZE_MAX)) {
        return USB_RSP_CODE_MEMORY_ERROR;
    }

    log_info("reading from I2C-%u statistics memory at address 0x%02X (%u entries)",
        iface, addr, len);
    mem_stat(mem_get(iface), addr, out, len);

    return USB_RSP_CODE_OK;
}

static usb_rsp_code_t usb_info_callback(uint8_t* out, size_t max_len, size_t* out_size) {
    assert(out != NULL);
    assert(out_size != NULL);
    assert(max_len > 0);

    log_info("reqested device info");
    *out_size = snprintf((char*) out, max_len, "%s;%s;%s;%u",
        g_picoslave_serial,
        GIT_VERSION,
        USB_VERSION,
        PICOSLAVE_I2C_NUM_SLAVES
    );

    return USB_RSP_CODE_OK;
}

static usb_rsp_code_t usb_reset_callback(void) {
    static const uint8_t delay = 1;

    log_warn("system reset in %dms", delay);
    add_alarm_in_ms(delay, timer_reset_handler, NULL, false);

    return USB_RSP_CODE_OK;
}


/**************************************************************************************************/
/* Private implementation */
/**************************************************************************************************/

static int64_t timer_reset_handler(alarm_id_t id, void* user_data) {
    NVIC_SystemReset();
    return 0;
}

static void print_reset_reason(void) {
    uint32_t chip_reset = vreg_and_chip_reset_hw->chip_reset;
    printf("  + reset:          ");
    if (chip_reset & VREG_AND_CHIP_RESET_CHIP_RESET_PSM_RESTART_FLAG_BITS) {
        printf("PSM ");
    }
    if (chip_reset & VREG_AND_CHIP_RESET_CHIP_RESET_HAD_PSM_RESTART_BITS) {
        printf("DEBUG ");
    }
    if (chip_reset & VREG_AND_CHIP_RESET_CHIP_RESET_HAD_RUN_BITS) {
        printf("PIN ");
    }
    if (chip_reset & VREG_AND_CHIP_RESET_CHIP_RESET_HAD_POR_BITS) {
        printf("POR/BOD ");
    }
    if (watchdog_caused_reboot()) {
        printf("WATCHDOG ");
    }
    printf("\n");
}

static void print_chip_info(void) {
    uint32_t chip_id_reg = *((io_ro_32*)(SYSINFO_BASE + SYSINFO_CHIP_ID_OFFSET));
    uint32_t revsion = \
        (chip_id_reg & SYSINFO_CHIP_ID_REVISION_BITS) >> SYSINFO_CHIP_ID_REVISION_LSB;
    uint32_t part = (chip_id_reg & SYSINFO_CHIP_ID_PART_BITS) >> SYSINFO_CHIP_ID_PART_LSB;
    uint32_t manufacturer = \
        (chip_id_reg & SYSINFO_CHIP_ID_MANUFACTURER_BITS) >> SYSINFO_CHIP_ID_MANUFACTURER_LSB;
    uint32_t gitref_reg = *((io_ro_32*)(SYSINFO_BASE + SYSINFO_GITREF_RP2040_OFFSET));
    printf("  + serial:         %s\n", g_picoslave_serial);
    printf("  + usb:            %04"PRIx16":%04"PRIx16"\n",
                                PICOSLAVE_USB_IDVENDOR, PICOSLAVE_USB_IDPRODUCT);
    printf("  + chip (r/p/m):   %"PRIX32"/%"PRIX32"/%"PRIX32"\n", revsion, part, manufacturer);
    printf("  + source rev:     g%"PRIx32"\n", gitref_reg);
}

/**************************************************************************************************/
/* Public Implementation */
/**************************************************************************************************/

int main() {

    board_init();
    led_init();
    led_blink();
    stdio_init_all();

    printf("\x1b[0m\n+++ PICOSLAVE (%s) +++\n", GIT_VERSION);
    pico_get_unique_board_id_string(g_picoslave_serial, sizeof(g_picoslave_serial));
    print_reset_reason();
    print_chip_info();

    log_init(LOG_LEVEL);
    printf("  + log-level:      %s\n", log_level_to_str(LOG_LEVEL));
    printf("  + watchdog:       %s\n", WATCHDOGOFF ? "disabled" : "enabled");

    // init shared memory
    mem_init();

    // init I2C
    di2c_init();

    // init USB DEVICE
    log_debug("initializing USB device...");
    usb_init(&USB_CTRL);
    tusb_init();
    log_info("USB device initialized");

#if !WATCHDOGOFF
    // Note: watchdog is disabled when debugging
    watchdog_enable(WATCHDOG_TIMEOUT, 1);
#endif /* WATCHDOGOFF */

    while (true) {
        tud_task(); // tinyusb device task
        usb_task();
        watchdog_update();
    }

    sleep_until(at_the_end_of_time);
}
