#ifndef PICOSLAVE_CONFIG_H_
#define PICOSLAVE_CONFIG_H_

extern char g_picoslave_serial[];

/**************************************************************************************************/
/* USB Config */
/**************************************************************************************************/

#define PICOSLAVE_USB_IDVENDOR            (0x1209u)
#define PICOSLAVE_USB_IDPRODUCT           (0xC12Cu)
#define PICOSLAVE_USB_MANUFACTURER        "NONE"
#define PICOSLAVE_USB_PRODUCT             "PicoSlave"
#define PICOSLAVE_USB_IN_EP_ADDR          (0x85u)
#define PICOSLAVE_USB_OUT_EP_ADDR         (0x04u)
#define PICOSLAVE_USB_PACKET_SIZE         (64u)

#define PICOSLAVE_CDC_NOTIFICATION_EP_NUM (0x81u)
#define PICOSLAVE_CDC_DATA_OUT_EP_NUM     (0x02u)
#define PICOSLAVE_CDC_DATA_IN_EP_NUM      (0x83u)


/**************************************************************************************************/
/* I2C Config */
/**************************************************************************************************/

#define PICOSLAVE_I2C_NUM_SLAVES (2u)
#define PICOSLAVE_I2C_0_SDA_PIN  (PICO_DEFAULT_I2C_SDA_PIN) // 4
#define PICOSLAVE_I2C_0_SCL_PIN  (PICO_DEFAULT_I2C_SCL_PIN) // 5
#define PICOSLAVE_I2C_1_SDA_PIN  (6u)
#define PICOSLAVE_I2C_1_SCL_PIN  (7u)


/**************************************************************************************************/
/* Memory Config */
/**************************************************************************************************/
#define MEMORY_SIZE_MAX   (256u)
#define MEMORY_WIDTH_MAX  (4u)
#define MEMORY_AREA_MAX   (MEMORY_SIZE_MAX * MEMORY_WIDTH_MAX)


/**************************************************************************************************/
/* Logging Config (internal) */
/**************************************************************************************************/
/* Macro acrobatics to turn the CMake passed variable LOGLEVEL, which (hopefully) contains
 * values from [TRACE, DEBUG, INFO, WARN, ERROR, FATAL], into the corresponding macros defined
 * by log.h, which are the same but prefixed with "LOG_". */
#define LOG_C_LEVEL(level) LOG_##level
#define MAKE_LEVEL(level) LOG_C_LEVEL(level)
#define LOG_LEVEL MAKE_LEVEL(LOGLEVEL)


#endif /* PICOSLAVE_CONFIG_H_ */
