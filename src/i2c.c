#include <string.h>

#include <pico/stdlib.h>

#include "i2c_fifo.h"
#include "i2c_slave.h"
#include "i2c.h"
#include "log.h"
#include "mem.h"
#include "protocol.h"

#include "led.h"
#include "picoslave_config.h"


/**************************************************************************************************/
/* Definitions */
/**************************************************************************************************/

/**************************************************************************************************/
/* Types */
/**************************************************************************************************/

typedef struct {
    size_t mem_address;
    bool mem_address_written;
} di2c_slave_context_t;

typedef struct {
    uint8_t iface;
    i2c_inst_t* i2c;
    uint sda_pin;
    uint scl_pin;
    i2c_slave_handler_t handler;
} di2c_slave_init_t;

typedef struct {
    mem_t* mem;
    uint8_t i2c_address;
    di2c_slave_context_t* context;
    di2c_slave_init_t* init;
} di2c_slave_config_t;


/**************************************************************************************************/
/* Prototypes */
/**************************************************************************************************/

static void di2c_slave_handler_0(i2c_inst_t *i2c, i2c_slave_event_t event);
static void di2c_slave_handler_1(i2c_inst_t *i2c, i2c_slave_event_t event);


/**************************************************************************************************/
/* Globals */
/**************************************************************************************************/

static di2c_slave_context_t DI2C_SLAVE_CONTEXT[PICOSLAVE_I2C_NUM_SLAVES];

static di2c_slave_init_t DI2C_SLAVE_INIT[PICOSLAVE_I2C_NUM_SLAVES] = {
    {
        .iface = 0,
        .i2c = i2c0,
        .sda_pin = PICOSLAVE_I2C_0_SDA_PIN,
        .scl_pin = PICOSLAVE_I2C_0_SCL_PIN,
        .handler = &di2c_slave_handler_0,
    },
    {
        .iface = 1,
        .i2c = i2c1,
        .sda_pin = PICOSLAVE_I2C_1_SDA_PIN,
        .scl_pin = PICOSLAVE_I2C_1_SCL_PIN,
        .handler = &di2c_slave_handler_1,
    },
};

static di2c_slave_config_t DI2C_SLAVE_CONFIG[PICOSLAVE_I2C_NUM_SLAVES] = {
    {
        .i2c_address = 0x00,
        .context = &DI2C_SLAVE_CONTEXT[0],
        .init = &DI2C_SLAVE_INIT[0],
    },
    {
        .i2c_address = 0x00,
        .context = &DI2C_SLAVE_CONTEXT[1],
        .init = &DI2C_SLAVE_INIT[1],
    },
};


/**************************************************************************************************/
/* Private Implementation */
/**************************************************************************************************/

// Our handler is called from the I2C ISR, so it must complete quickly. Blocking calls /
// printing to stdio may interfere with interrupt handling.
static void di2c_slave_handler(i2c_inst_t *i2c, i2c_slave_event_t event, di2c_slave_config_t* config) {
    uint8_t rxtx_byte;
    di2c_slave_context_t* context = config->context;
    switch (event) {
    case I2C_SLAVE_START: // master has signalled Start
        log_trace("[i2c%u@%02xh] st", config->init->iface, config->i2c_address);
        context->mem_address_written = false;
        break;
    case I2C_SLAVE_RECEIVE: // master has written some data
        rxtx_byte = i2c_read_byte(i2c);
        if (!context->mem_address_written) {
            log_trace("[i2c%u@%02xh] rx: @%=%02xh", config->init->iface, config->i2c_address, rxtx_byte);
            // writes always start with the memory address
            if (rxtx_byte >= config->mem->size * config->mem->width) {
                log_error("address %02xh out of range [0..%02h]. Reset to 0.", rxtx_byte,
                    config->mem->size * config->mem->width);
                context->mem_address = 0;
            } else {
                context->mem_address = rxtx_byte * config->mem->width;
            }
            context->mem_address_written = true;
        } else {
            // save into memory
            mem_write(config->mem, context->mem_address, &rxtx_byte, 1, true);
            context->mem_address++;
            if (context->mem_address >= config->mem->size * config->mem->width) {
                context->mem_address = 0;
            }
            log_trace("[i2c%u@%02xh] rx: %02Xh > @%02xh", config->init->iface, config->i2c_address,
                                                           rxtx_byte, context->mem_address);
        }
        break;
    case I2C_SLAVE_REQUEST: // master is requesting data
        // load from memory
        mem_read(config->mem, context->mem_address, &rxtx_byte, 1, true);
        i2c_write_byte(i2c, rxtx_byte);
        context->mem_address++;
        if (context->mem_address >= config->mem->size * config->mem->width) {
            context->mem_address = 0;
        }
        log_trace("[i2c%u@%02xh] rq: @%02xh > %02Xh", config->init->iface, config->i2c_address,
            context->mem_address, rxtx_byte);
        break;
    case I2C_SLAVE_FINISH: // master has finished
        log_trace("[i2c%u@%02xh] fx", config->init->iface, config->i2c_address);
        context->mem_address_written = false;
        led_blink();
        break;
    default:
        break;
    }
}

static void di2c_slave_handler_0(i2c_inst_t *i2c, i2c_slave_event_t event) {
    di2c_slave_handler(i2c, event, &DI2C_SLAVE_CONFIG[0]);
}

static void di2c_slave_handler_1(i2c_inst_t *i2c, i2c_slave_event_t event) {
    di2c_slave_handler(i2c, event, &DI2C_SLAVE_CONFIG[1]);
}

static void di2c_init_slave_io(di2c_slave_init_t const* init) {
    gpio_init(init->sda_pin);
    gpio_set_function(init->sda_pin, GPIO_FUNC_I2C);
    gpio_pull_up(init->sda_pin);

    gpio_init(init->scl_pin);
    gpio_set_function(init->scl_pin, GPIO_FUNC_I2C);
    gpio_pull_up(init->scl_pin);
}

static void di2c_init_blocker_io(uint pin) {
    gpio_init(pin); // default function is GPIO_FUNC_SIO
    gpio_put(pin, false);
    gpio_set_dir(pin, GPIO_OUT);
}

static void di2c_deinit_io(di2c_slave_init_t const* init) {
    gpio_init(init->sda_pin);
    gpio_disable_pulls(init->sda_pin);
    gpio_set_dir(init->sda_pin, GPIO_IN);

    gpio_init(init->scl_pin);
    gpio_disable_pulls(init->scl_pin);
    gpio_set_dir(init->scl_pin, GPIO_IN);
}


/**************************************************************************************************/
/* Public Implementation */
/**************************************************************************************************/

void di2c_init(void) {
    for (unsigned i = 0; i < PICOSLAVE_I2C_NUM_SLAVES; i++) {
        di2c_deinit(i);
    }
    log_info("I2C interfaces initialized");
}

void di2c_init_slave(uint8_t iface, uint8_t slave_address, uint32_t baudrate, mem_t* mem) {
    assert(iface < PICOSLAVE_I2C_NUM_SLAVES);
    assert(slave_address > 0);
    assert(slave_address <= I2C_SLAVE_FUNC_SLAVE);

    di2c_init_slave_io(&DI2C_SLAVE_INIT[iface]);

    di2c_slave_config_t* config = &DI2C_SLAVE_CONFIG[iface];

    memset(config->context, 0, sizeof(di2c_slave_context_t));
    config->i2c_address = slave_address;
    config->mem = mem;

    uint bus_freq = i2c_init(config->init->i2c, baudrate);
    i2c_slave_init(config->init->i2c, config->i2c_address, config->init->handler);

    log_info("[i2c%u@%02xh] initialized for %d Hz", iface, slave_address, bus_freq);
}

void di2c_init_blocker(uint8_t iface, bool scl, bool sda) {
    assert(iface < PICOSLAVE_I2C_NUM_SLAVES);

    di2c_slave_config_t const* config = &DI2C_SLAVE_CONFIG[iface];

    if (scl) {
        di2c_init_blocker_io(config->init->scl_pin);
    }
    if (sda) {
        di2c_init_blocker_io(config->init->sda_pin);
    }
}

void di2c_deinit(uint8_t iface) {
    assert(iface < PICOSLAVE_I2C_NUM_SLAVES);

    di2c_slave_config_t* config = &DI2C_SLAVE_CONFIG[iface];

    memset(config->context, 0, sizeof(di2c_slave_context_t));
    config->i2c_address = I2C_SLAVE_FUNC_RESET;
    config->mem = NULL;

    i2c_slave_deinit(config->init->i2c);
    i2c_deinit(config->init->i2c);

    di2c_deinit_io(config->init);
}
