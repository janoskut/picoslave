#ifndef DI2C_H_
#define DI2C_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "i2c_slave/i2c_slave.h"
#include "mem.h"
#include "protocol.h"


/**************************************************************************************************/
/* Definitions */
/**************************************************************************************************/


/**************************************************************************************************/
/* Types */
/**************************************************************************************************/

typedef enum i2c_slave_function_t {
    I2C_SLAVE_FUNC_RESET   = 0x00,
    I2C_SLAVE_FUNC_SLAVE   = 0x7F,
    I2C_SLAVE_FUNC_BLOCKER = 0xF1,
} i2c_slave_function_t;


/**************************************************************************************************/
/* Public Interface */
/**************************************************************************************************/
void di2c_init(void);
void di2c_init_slave(uint8_t iface, uint8_t slave_address, uint32_t baudrate, mem_t* mem);
void di2c_init_blocker(uint8_t iface, bool scl, bool sda);
void di2c_deinit(uint8_t iface);


#endif /* DI2C_H_ */
