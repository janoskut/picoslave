#include <i2c_slave.h>
#include <hardware/irq.h>
#include <hardware/sync.h>

#include "i2c_fifo.h"

#include "log.h"

typedef struct i2c_slave_t
{
    i2c_inst_t *i2c;
    i2c_slave_handler_t handler;
} i2c_slave_t;

static i2c_slave_t i2c0_slave_context;
static i2c_slave_t i2c1_slave_context;

static void i2c_slave_irq_handler(i2c_slave_t* slave) {
    i2c_inst_t *i2c = slave->i2c;
    i2c_hw_t *hw = i2c_get_hw(i2c);

    uint32_t intr_stat = hw->intr_stat;
    if (intr_stat == 0) {
        return;
    }
    if (intr_stat & I2C_IC_INTR_STAT_R_RX_FULL_BITS) {
        while (hw->status & I2C_IC_STATUS_RFNE_BITS) {
            // FIXME: not so ideal that we have to rely on the handler to fetch the "i2c_read_byte()"
            slave->handler(i2c, I2C_SLAVE_RECEIVE);
        }
    }
    if (intr_stat & I2C_IC_INTR_STAT_R_RD_REQ_BITS) {
        slave->handler(i2c, I2C_SLAVE_REQUEST);
        hw->clr_rd_req;
    }
    if (intr_stat & I2C_IC_INTR_STAT_R_TX_ABRT_BITS) {
        slave->handler(slave->i2c, I2C_SLAVE_START);
        hw->clr_tx_abrt;
    }
    if (intr_stat & I2C_IC_INTR_STAT_R_START_DET_BITS) {
        /* Note: despite doc for I2C_IC_RAW_INTR_STAT_START_DET saysing so, it doesn't seem like
         *       the START_DET interrupt is set on a RESTART condition. */
        slave->handler(slave->i2c, I2C_SLAVE_START);
        hw->clr_start_det;
    }
    if (intr_stat & I2C_IC_INTR_STAT_R_RX_UNDER_BITS) {
        log_warn("I2C RX UNDER");
        hw->clr_rx_under;
    }
    if (intr_stat & I2C_IC_INTR_STAT_R_STOP_DET_BITS) {
        slave->handler(slave->i2c, I2C_SLAVE_FINISH);
        hw->clr_stop_det;
    }
}

static void __not_in_flash_func(i2c0_slave_irq_handler)() {
    i2c_slave_irq_handler(&i2c0_slave_context);
}

static void __not_in_flash_func(i2c1_slave_irq_handler)() {
    i2c_slave_irq_handler(&i2c1_slave_context);
}

void i2c_slave_init(i2c_inst_t *i2c, uint8_t address, i2c_slave_handler_t handler) {
    invalid_params_if(I2C, i2c != i2c0 && i2c != i2c1);
    invalid_params_if(I2C, handler == NULL);

    i2c_hw_t *hw = i2c_get_hw(i2c);
    i2c_slave_t *slave = (i2c == i2c0 ? &i2c0_slave_context : &i2c1_slave_context);
    slave->i2c = i2c;
    slave->handler = handler;

    i2c_set_slave_mode(i2c, true, address);
    hw->enable = 0;
    // enable clock stretching on RX FIFO full. IP needs to be disabled
    hw_set_bits(&hw->con, I2C_IC_CON_RX_FIFO_FULL_HLD_CTRL_BITS
                        | I2C_IC_CON_STOP_DET_IFADDRESSED_BITS);
    hw->enable = 1;

    // unmask necessary interrupts
    hw->intr_mask = I2C_IC_INTR_MASK_M_RX_FULL_BITS
                  | I2C_IC_INTR_MASK_M_RD_REQ_BITS
                  | I2C_IC_INTR_MASK_M_TX_ABRT_BITS
                  | I2C_IC_INTR_MASK_M_START_DET_BITS
                  | I2C_IC_INTR_MASK_M_RX_UNDER_BITS
                  | I2C_IC_INTR_MASK_M_STOP_DET_BITS
                //   | I2C_IC_INTR_MASK_M_RX_DONE_BITS  //< not needed, we get the STOP event
                //   | I2C_IC_INTR_MASK_M_RESTART_DET_BITS //< not needed, nothing to be done at restart
                  ;


    if (i2c == i2c0) {
        irq_set_exclusive_handler(I2C0_IRQ, i2c0_slave_irq_handler);
        irq_set_enabled(I2C0_IRQ, true);
    } else {
        irq_set_exclusive_handler(I2C1_IRQ, i2c1_slave_irq_handler);
        irq_set_enabled(I2C1_IRQ, true);
    }
}

void i2c_slave_deinit(i2c_inst_t *i2c) {
    i2c_hw_t *hw = i2c_get_hw(i2c);
    i2c_slave_t *slave = (i2c == i2c0 ? &i2c0_slave_context : &i2c1_slave_context);
    invalid_params_if(I2C, i2c != i2c0 && i2c != i2c1);

    slave->i2c = NULL;
    slave->handler = NULL;

    if (i2c == i2c0) {
        irq_set_enabled(I2C0_IRQ, false);
        irq_remove_handler(I2C0_IRQ, i2c0_slave_irq_handler);
    } else {
        irq_set_enabled(I2C1_IRQ, false);
        irq_remove_handler(I2C1_IRQ, i2c1_slave_irq_handler);
    }

    hw->intr_mask = I2C_IC_INTR_MASK_RESET;

    i2c_set_slave_mode(i2c, false, 0);
}
