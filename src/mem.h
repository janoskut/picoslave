#ifndef MEM_H_
#define MEM_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>


/**************************************************************************************************/
/* Definitions */
/**************************************************************************************************/

/**************************************************************************************************/
/* Types */
/**************************************************************************************************/
typedef struct memory_s memory_t;

typedef struct  {
    size_t    size;
    uint8_t   width;
    memory_t* internal;
} mem_t;


/**************************************************************************************************/
/* Public Interface */
/**************************************************************************************************/

void mem_init(void);
mem_t* mem_config(uint8_t iface, size_t size, uint8_t width);
mem_t* mem_get(uint8_t iface);
void mem_clear(mem_t* mem, uint8_t reset_value);
void mem_read(mem_t* mem, uint16_t addr, uint8_t* out, size_t len, bool stat);
void mem_write(mem_t* mem, uint16_t addr, uint8_t const* data, size_t len, bool stat);
void mem_stat(mem_t* mem, uint16_t addr, uint8_t* out, size_t len);

#endif /* MEM_H_ */
