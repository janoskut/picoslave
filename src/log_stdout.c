#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include <pico/critical_section.h>

#include "log.h"


/**************************************************************************************************/
/* Definitions */
/**************************************************************************************************/

#ifdef LOGUSECOLOR
#define LOG_USE_COLOR 1
#endif /* LOGUSECOLOR */
#if LOGUSEPADDING
#define LOG_USE_PADDING 1
#endif /* LOG_USE_PADDING */


/**************************************************************************************************/
/* Types */
/**************************************************************************************************/

/**************************************************************************************************/
/* Prototypes */
/**************************************************************************************************/

/**************************************************************************************************/
/* Globals */
/**************************************************************************************************/

static int LOG_PADDING = 0;

static critical_section_t log_mutex;


/**************************************************************************************************/
/* Private Implementation */
/**************************************************************************************************/

static void log_stdout_callback(log_Event *ev) {

    static char buf[256];
    int file_len = snprintf(buf, sizeof(buf), "%s:%d:", ev->file, ev->line);
    buf[file_len] = '\0';

#if LOG_USE_PADDING == 1
    if (file_len > LOG_PADDING) {
        LOG_PADDING = file_len;
    }
#endif

    uint64_t time = to_us_since_boot(get_absolute_time());
    double ftime = time / (double) 1000;

#if LOG_USE_COLOR == 1
    static const char *level_colors[] = {
        "\x1b[94m", "\x1b[36m", "\x1b[32m", "\x1b[33m", "\x1b[31m", "\x1b[35m"
    };
    fprintf(
        ev->udata,
        "%s%-5s\x1b[0m \x1b[95m%014.03f\x1b[0m  \x1b[90m%*s\x1b[0m ",
        level_colors[ev->level],
        log_level_string(ev->level),
        ftime,
        LOG_PADDING,
        buf);
#else
    fprintf(ev->udata, "%-5s  %014.03f  %*s ",
        log_level_string(ev->level), ftime, LOG_PADDING, buf);
#endif
    vfprintf(ev->udata, ev->fmt, ev->ap);
    fprintf(ev->udata, "\n");
    fflush(ev->udata);
}

const char* log_level_to_str(uint level) {
    switch (level) {
        case LOG_TRACE:
            return "TRACE";
        case LOG_DEBUG:
            return "DEBUG";
        case LOG_INFO:
            return "INFO";
        case LOG_WARN:
            return "WARN";
        case LOG_ERROR:
            return "ERROR";
        case LOG_FATAL:
            return "FATAL";
    };
    return "UNKNOWN";
}

/** Implements log.h::log_LogFn */
static void log_lock(bool lock, void *udata) {
    assert(udata != NULL);
    critical_section_t* mutex = (critical_section_t*) udata;
    if (lock) {
        critical_section_enter_blocking(mutex);
    } else {
        critical_section_exit(mutex);
    }
}


/**************************************************************************************************/
/* Public Implementation */
/**************************************************************************************************/

void log_init(uint level) {
    critical_section_init(&log_mutex);
    log_set_lock(log_lock, &log_mutex);
    /* Set quiet, because the default implementation uses `stderr`, which we don't have.
     * Instead, register our own handler, which uses stdout. */
    log_set_quiet(true);
    log_add_callback(log_stdout_callback, stdout, level);
}
